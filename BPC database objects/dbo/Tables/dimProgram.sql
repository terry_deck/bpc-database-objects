﻿CREATE TABLE [dbo].[dimProgram] (
    [SEQ1]           NVARCHAR (50) NULL,
    [ID1]            NVARCHAR (20) NULL,
    [CALC1]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION1] NVARCHAR (50) NULL,
    [HLEVEL1]        NVARCHAR (2)  NULL,
    [SCALING1]       SMALLINT      NULL,
    [SEQ2]           NVARCHAR (50) NULL,
    [ID2]            NVARCHAR (20) NULL,
    [CALC2]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION2] NVARCHAR (50) NULL,
    [HLEVEL2]        NVARCHAR (2)  NULL,
    [SCALING2]       SMALLINT      NULL,
    [SEQ3]           NVARCHAR (50) NULL,
    [ID3]            NVARCHAR (20) NULL,
    [CALC3]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION3] NVARCHAR (50) NULL,
    [HLEVEL3]        NVARCHAR (2)  NULL,
    [SCALING3]       SMALLINT      NULL,
    [SEQ4]           NVARCHAR (50) NULL,
    [ID4]            NVARCHAR (20) NULL,
    [CALC4]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION4] NVARCHAR (50) NULL,
    [HLEVEL4]        NVARCHAR (2)  NULL,
    [SCALING4]       SMALLINT      NULL,
    [SEQ5]           NVARCHAR (50) NULL,
    [ID5]            NVARCHAR (20) NULL,
    [CALC5]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION5] NVARCHAR (50) NULL,
    [HLEVEL5]        NVARCHAR (2)  NULL,
    [SCALING5]       SMALLINT      NULL,
    [SEQ6]           NVARCHAR (50) NULL,
    [ID6]            NVARCHAR (20) NULL,
    [CALC6]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION6] NVARCHAR (50) NULL,
    [HLEVEL6]        NVARCHAR (2)  NULL,
    [SCALING6]       SMALLINT      NULL,
    [ORGANIZATION]   NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [ID1]
    ON [dbo].[dimProgram]([ID1] ASC);


GO
CREATE NONCLUSTERED INDEX [ID2]
    ON [dbo].[dimProgram]([ID2] ASC);


GO
CREATE NONCLUSTERED INDEX [ID3]
    ON [dbo].[dimProgram]([ID3] ASC);


GO
CREATE NONCLUSTERED INDEX [ID4]
    ON [dbo].[dimProgram]([ID4] ASC);


GO
CREATE NONCLUSTERED INDEX [ID5]
    ON [dbo].[dimProgram]([ID5] ASC);


GO
CREATE NONCLUSTERED INDEX [ID6]
    ON [dbo].[dimProgram]([ID6] ASC);

