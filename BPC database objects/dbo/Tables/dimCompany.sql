﻿CREATE TABLE [dbo].[dimCompany] (
    [SEQ1]           NVARCHAR (50) NULL,
    [ID1]            NVARCHAR (20) NULL,
    [CALC1]          NVARCHAR (1)  NULL,
    [CURRENCY1]      NVARCHAR (20) NULL,
    [EVDESCRIPTION1] NVARCHAR (50) NULL,
    [HLEVEL1]        NVARCHAR (2)  NULL,
    [INTCO1]         NVARCHAR (20) NULL,
    [PAR_CO1]        NVARCHAR (1)  NULL,
    [OWNER1]         NVARCHAR (20) NULL,
    [SEQ2]           NVARCHAR (50) NULL,
    [ID2]            NVARCHAR (20) NULL,
    [CALC2]          NVARCHAR (1)  NULL,
    [CURRENCY2]      NVARCHAR (20) NULL,
    [EVDESCRIPTION2] NVARCHAR (50) NULL,
    [HLEVEL2]        NVARCHAR (2)  NULL,
    [INTCO2]         NVARCHAR (20) NULL,
    [PAR_CO2]        NVARCHAR (1)  NULL,
    [OWNER2]         NVARCHAR (20) NULL,
    [SEQ3]           NVARCHAR (50) NULL,
    [ID3]            NVARCHAR (20) NULL,
    [CALC3]          NVARCHAR (1)  NULL,
    [CURRENCY3]      NVARCHAR (20) NULL,
    [EVDESCRIPTION3] NVARCHAR (50) NULL,
    [HLEVEL3]        NVARCHAR (2)  NULL,
    [INTCO3]         NVARCHAR (20) NULL,
    [PAR_CO3]        NVARCHAR (1)  NULL,
    [OWNER3]         NVARCHAR (20) NULL,
    [SEQ4]           NVARCHAR (50) NULL,
    [ID4]            NVARCHAR (20) NULL,
    [CALC4]          NVARCHAR (1)  NULL,
    [CURRENCY4]      NVARCHAR (20) NULL,
    [EVDESCRIPTION4] NVARCHAR (50) NULL,
    [HLEVEL4]        NVARCHAR (2)  NULL,
    [INTCO4]         NVARCHAR (20) NULL,
    [PAR_CO4]        NVARCHAR (1)  NULL,
    [OWNER4]         NVARCHAR (20) NULL,
    [SEQ5]           NVARCHAR (50) NULL,
    [ID5]            NVARCHAR (20) NULL,
    [CALC5]          NVARCHAR (1)  NULL,
    [CURRENCY5]      NVARCHAR (20) NULL,
    [EVDESCRIPTION5] NVARCHAR (50) NULL,
    [HLEVEL5]        NVARCHAR (2)  NULL,
    [INTCO5]         NVARCHAR (20) NULL,
    [PAR_CO5]        NVARCHAR (1)  NULL,
    [OWNER5]         NVARCHAR (20) NULL,
    [SEQ6]           NVARCHAR (50) NULL,
    [ID6]            NVARCHAR (20) NULL,
    [CALC6]          NVARCHAR (1)  NULL,
    [CURRENCY6]      NVARCHAR (20) NULL,
    [EVDESCRIPTION6] NVARCHAR (50) NULL,
    [HLEVEL6]        NVARCHAR (2)  NULL,
    [INTCO6]         NVARCHAR (20) NULL,
    [PAR_CO6]        NVARCHAR (1)  NULL,
    [OWNER6]         NVARCHAR (20) NULL,
    [SEQ7]           NVARCHAR (50) NULL,
    [ID7]            NVARCHAR (20) NULL,
    [CALC7]          NVARCHAR (1)  NULL,
    [CURRENCY7]      NVARCHAR (20) NULL,
    [EVDESCRIPTION7] NVARCHAR (50) NULL,
    [HLEVEL7]        NVARCHAR (2)  NULL,
    [INTCO7]         NVARCHAR (20) NULL,
    [PAR_CO7]        NVARCHAR (1)  NULL,
    [OWNER7]         NVARCHAR (20) NULL,
    [SEQ8]           NVARCHAR (50) NULL,
    [ID8]            NVARCHAR (20) NULL,
    [CALC8]          NVARCHAR (1)  NULL,
    [CURRENCY8]      NVARCHAR (20) NULL,
    [EVDESCRIPTION8] NVARCHAR (50) NULL,
    [HLEVEL8]        NVARCHAR (2)  NULL,
    [INTCO8]         NVARCHAR (20) NULL,
    [PAR_CO8]        NVARCHAR (1)  NULL,
    [OWNER8]         NVARCHAR (20) NULL,
    [ORGANIZATION]   NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [ID1]
    ON [dbo].[dimCompany]([ID1] ASC);


GO
CREATE NONCLUSTERED INDEX [ID2]
    ON [dbo].[dimCompany]([ID2] ASC);


GO
CREATE NONCLUSTERED INDEX [ID3]
    ON [dbo].[dimCompany]([ID3] ASC);


GO
CREATE NONCLUSTERED INDEX [ID4]
    ON [dbo].[dimCompany]([ID4] ASC);


GO
CREATE NONCLUSTERED INDEX [ID5]
    ON [dbo].[dimCompany]([ID5] ASC);


GO
CREATE NONCLUSTERED INDEX [ID6]
    ON [dbo].[dimCompany]([ID6] ASC);


GO
CREATE NONCLUSTERED INDEX [ID7]
    ON [dbo].[dimCompany]([ID7] ASC);


GO
CREATE NONCLUSTERED INDEX [ID8]
    ON [dbo].[dimCompany]([ID8] ASC);

