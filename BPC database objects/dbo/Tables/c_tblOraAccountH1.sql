﻿CREATE TABLE [dbo].[c_tblOraAccountH1] (
    [ID]                VARCHAR (20) NULL,
    [EVDESCRIPTION]     VARCHAR (50) NULL,
    [FRDESCRIPTION]     VARCHAR (50) NULL,
    [PARENT]            VARCHAR (20) NULL,
    [ORCL_ACCOUNT_TYPE] VARCHAR (1)  NULL,
    [ENABLE]            VARCHAR (1)  NULL
);

