﻿CREATE TABLE [dbo].[c_tblBPC_DW_FILE1] (
    [primary_agent_chf_district] NVARCHAR (255)  NULL,
    [product_type]               NVARCHAR (255)  NULL,
    [effective_date]             NVARCHAR (10)   NULL,
    [region]                     NVARCHAR (255)  NULL,
    [pol_type_new_ytd_cnt]       NUMERIC (15, 2) NULL,
    [new_prem_ytd_amt]           NUMERIC (15, 2) NULL,
    [claim_cov_cnt]              NUMERIC (15, 2) NULL,
    [pol_type_inf_cur_cnt]       NUMERIC (15, 2) NULL,
    [new_prem_adj_ytd_amt]       NUMERIC (15, 2) NULL
);

