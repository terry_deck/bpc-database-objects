﻿CREATE TABLE [dbo].[c_tblBPCOracleDimHir] (
    [Dim_ID]              NVARCHAR (20)  NOT NULL,
    [Dim_Hir_ID]          SMALLINT       NOT NULL,
    [Oracle_Dim_Name]     NVARCHAR (100) NOT NULL,
    [Oracle_Dim_Hir_Code] NVARCHAR (30)  NOT NULL,
    [Oracle_Dim_Tbl_Name] NVARCHAR (50)  NOT NULL,
    [Dim_Last_Chg_Dt]     DATETIME       NOT NULL
);

