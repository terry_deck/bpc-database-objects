﻿CREATE TABLE [dbo].[aBPC_BATCH_STATUS] (
    [runtime] DATETIME       NOT NULL,
    [package] NVARCHAR (50)  NULL,
    [status]  NVARCHAR (10)  NULL,
    [message] NVARCHAR (100) NULL
);

