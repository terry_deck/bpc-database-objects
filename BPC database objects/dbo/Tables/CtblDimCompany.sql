﻿CREATE TABLE [dbo].[CtblDimCompany] (
    [AGREGAT]      NVARCHAR (20) NULL,
    [ID]           NVARCHAR (20) NULL,
    [Organization] NVARCHAR (3)  NULL,
    [Niv]          INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_AGREGAT]
    ON [dbo].[CtblDimCompany]([AGREGAT] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ID]
    ON [dbo].[CtblDimCompany]([ID] ASC);

