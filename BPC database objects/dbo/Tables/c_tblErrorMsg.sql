﻿CREATE TABLE [dbo].[c_tblErrorMsg] (
    [ID]        NUMERIC (18)   NOT NULL,
    [Task_ID]   NUMERIC (18)   NOT NULL,
    [ErrorType] INT            NOT NULL,
    [ErrorMsg]  NVARCHAR (250) NULL,
    [ReportOn]  DATETIME       NULL,
    CONSTRAINT [FK_c_tblErrorMsg_c_tblBPCAdminTask] FOREIGN KEY ([Task_ID]) REFERENCES [dbo].[c_tblBPCAdminTask] ([Task_ID]),
    CONSTRAINT [FK_c_tblErrorMsg_c_tblCdErrorType] FOREIGN KEY ([ErrorType]) REFERENCES [dbo].[c_tblCdErrorType] ([ID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TaskID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'c_tblErrorMsg', @level2type = N'CONSTRAINT', @level2name = N'FK_c_tblErrorMsg_c_tblBPCAdminTask';

