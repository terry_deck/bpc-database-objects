﻿CREATE TABLE [dbo].[c_tblSrcDimAccount] (
    [ID]                NVARCHAR (20) NOT NULL,
    [EVDESCRIPTION]     NVARCHAR (50) NULL,
    [FRDESCRIPTION]     NVARCHAR (50) NULL,
    [PARENTH1]          NVARCHAR (20) NULL,
    [PARENTH2]          NVARCHAR (20) NULL,
    [PARENTH3]          NVARCHAR (20) NULL,
    [ACCTYPE]           NVARCHAR (3)  NULL,
    [ORCL_ACCOUNT_TYPE] NVARCHAR (10) NULL,
    [RATETYPE]          NVARCHAR (10) NULL,
    [ENABLE]            VARCHAR (1)   NULL
);

