﻿CREATE TABLE [dbo].[c_tblBPC_DW_FILE2] (
    [yr]                         NVARCHAR (4)    NOT NULL,
    [month]                      NVARCHAR (9)    NOT NULL,
    [primary_agent_chf_district] NVARCHAR (255)  NOT NULL,
    [product_type]               NVARCHAR (255)  NOT NULL,
    [new_vehicle_count]          NUMERIC (15, 2) NULL,
    [new_policy_count]           NUMERIC (15, 2) NULL,
    [new_premium]                NUMERIC (15, 2) NULL,
    [vif]                        NUMERIC (15, 2) NULL,
    [pif]                        NUMERIC (15, 2) NULL
);

