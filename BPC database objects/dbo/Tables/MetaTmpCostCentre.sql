﻿CREATE TABLE [dbo].[MetaTmpCostCentre] (
    [ID]                     NVARCHAR (20) NULL,
    [NEWID]                  NVARCHAR (20) NULL,
    [EVDESCRIPTION]          NVARCHAR (50) NULL,
    [FRDESCRIPTION]          NVARCHAR (50) NULL,
    [PARENTH1]               NVARCHAR (20) NULL,
    [PARENTH2]               NVARCHAR (20) NULL,
    [PARENTH3]               NVARCHAR (20) NULL,
    [PARENTH4]               NVARCHAR (20) NULL,
    [INTCO]                  NVARCHAR (20) NULL,
    [SCALING]                NVARCHAR (2)  NULL,
    [CGIC_REGION_CCT]        NVARCHAR (20) NULL,
    [REGION_SALES]           NVARCHAR (1)  NULL,
    [REGION_VP]              NVARCHAR (1)  NULL,
    [SAL_CCT]                NVARCHAR (1)  NULL,
    [ENABLE]                 NVARCHAR (1)  NULL,
    [COSECO_CCT]             NVARCHAR (2)  NULL,
    [CALC]                   NVARCHAR (1)  NULL,
    [DIMCALC]                NVARCHAR (1)  NULL,
    [ISBASEMEM]              NVARCHAR (1)  NULL,
    [HIR]                    NVARCHAR (50) NULL,
    [META_NUM_DISTINCT_RECS] NUMERIC (18)  NULL,
    [autometa_hier_lvl]      INT           NULL
);

