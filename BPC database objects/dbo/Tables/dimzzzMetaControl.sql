﻿CREATE TABLE [dbo].[dimzzzMetaControl] (
    [SEQ1]           NVARCHAR (50) NULL,
    [CALC1]          NVARCHAR (1)  NULL,
    [EVDESCRIPTION1] NVARCHAR (50) NULL,
    [HLEVEL1]        NVARCHAR (2)  NULL,
    [SCALING1]       SMALLINT      NULL,
    [ID1]            NVARCHAR (20) DEFAULT ('') NULL,
    [ORGANIZATION]   NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [ID1]
    ON [dbo].[dimzzzMetaControl]([ID1] ASC);

