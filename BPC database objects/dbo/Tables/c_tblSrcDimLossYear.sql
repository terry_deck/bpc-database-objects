﻿CREATE TABLE [dbo].[c_tblSrcDimLossYear] (
    [ID]            NVARCHAR (20) NOT NULL,
    [EVDESCRIPTION] NVARCHAR (50) NULL,
    [FRDESCRIPTION] NVARCHAR (50) NULL,
    [PARENTH1]      NVARCHAR (20) NULL,
    [ORCL_ID]       NVARCHAR (4)  NULL,
    [ENABLE]        VARCHAR (1)   NULL
);

