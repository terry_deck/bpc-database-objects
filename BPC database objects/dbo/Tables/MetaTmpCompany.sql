﻿CREATE TABLE [dbo].[MetaTmpCompany] (
    [ID]                     NVARCHAR (20) NULL,
    [NEWID]                  NVARCHAR (20) NULL,
    [EVDESCRIPTION]          NVARCHAR (50) NULL,
    [FRDESCRIPTION]          NVARCHAR (50) NULL,
    [PARENTH1]               NVARCHAR (20) NULL,
    [PARENTH2]               NVARCHAR (20) NULL,
    [CURRENCY]               NVARCHAR (20) NULL,
    [INTCO]                  NVARCHAR (20) NULL,
    [FX_TYPE]                NVARCHAR (1)  NULL,
    [SHORT_NAME]             NVARCHAR (10) NULL,
    [BUDGET_LEVEL]           NVARCHAR (1)  NULL,
    [PAR_CO]                 NVARCHAR (1)  NULL,
    [ENABLE]                 NVARCHAR (1)  NULL,
    [OWNER]                  NVARCHAR (20) NULL,
    [CALC]                   NVARCHAR (1)  NULL,
    [DIMCALC]                NVARCHAR (1)  NULL,
    [ISBASEMEM]              NVARCHAR (1)  NULL,
    [HIR]                    NVARCHAR (50) NULL,
    [META_NUM_DISTINCT_RECS] NUMERIC (18)  NULL,
    [autometa_hier_lvl]      INT           NULL
);

