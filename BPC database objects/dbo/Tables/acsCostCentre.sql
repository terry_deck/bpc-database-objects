﻿CREATE TABLE [dbo].[acsCostCentre] (
    [APP]       NVARCHAR (30)  NOT NULL,
    [ProfileID] NVARCHAR (100) NOT NULL,
    [Member]    NVARCHAR (20)  NOT NULL,
    [RW]        NVARCHAR (1)   NULL
);


GO
CREATE CLUSTERED INDEX [CIDX_CostCentre_01]
    ON [dbo].[acsCostCentre]([ProfileID] ASC, [APP] ASC, [Member] ASC);

