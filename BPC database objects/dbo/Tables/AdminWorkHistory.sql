﻿CREATE TABLE [dbo].[AdminWorkHistory] (
    [WorkID]      VARCHAR (50)  NOT NULL,
    [TaskID]      VARCHAR (50)  NOT NULL,
    [SubTaskID]   VARCHAR (50)  NOT NULL,
    [Success]     VARCHAR (50)  NOT NULL,
    [Message]     VARCHAR (255) NOT NULL,
    [StartTime]   DATETIME      NOT NULL,
    [EndTime]     DATETIME      NULL,
    [EndWorkCode] BIT           NOT NULL
);

