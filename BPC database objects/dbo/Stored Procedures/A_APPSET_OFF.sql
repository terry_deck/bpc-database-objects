﻿



CREATE procedure [dbo].[A_APPSET_OFF]

as	

--Appset Status Off Stored Procedure
--This stored procedure sets the application set status to OFF.
--It should be run at the beginning of a processing cycle.

UPDATE tblDefaults
SET Value='0'
WHERE KeyID='AVAILABLEFLAG'


