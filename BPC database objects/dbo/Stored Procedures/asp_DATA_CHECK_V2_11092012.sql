﻿
CREATE procedure [dbo].[asp_DATA_CHECK_V2_11092012]

AS

--------------------------------BEGIN DATA CHECK-----------------------------------------------------

IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES	 WHERE TABLE_NAME = 'TMPBPC_STAGE')


BEGIN
CREATE TABLE dbo.TMPBPC_STAGE 

--CREATE TEMORY TABLE FOR BUILDING THE REPORT
(
[LEDGER_ID]   NVARCHAR(20),
[COMPANY]     NVARCHAR(20),
[COSTCENTRE]  NVARCHAR(20),
[ACCOUNT]     NVARCHAR(20),
[INTCO]       NVARCHAR(20),
[PROVINCE]    NVARCHAR(20),
[PROGRAM]     NVARCHAR(20),
[PRODUCT]     NVARCHAR(20),
[LOB]         NVARCHAR(20),
[PAR]         NVARCHAR(20),
[PROJECT]     NVARCHAR(20),
[TREATY]      NVARCHAR(20),
[REINSURER]   NVARCHAR(20),
[LOSSYEAR]    NVARCHAR(20),
[A_TYPE]      NVARCHAR(20),
[CURRENCY]    NVARCHAR(20),
[A_FLAG]      NVARCHAR(20),
[PERIOD_NAME] NVARCHAR(20),
[PERIOD_YEAR] NVARCHAR(20),
[PERIOD_NUM]  NVARCHAR(20),
[SOURCE]      NVARCHAR(20),
[DATASRC]     NVARCHAR(20),
[CATEGORY]    NVARCHAR(20),
[TIME]        NVARCHAR(20),
[TIMEID]      NVARCHAR(20),
[SIGNEDDATA]  DECIMAL(25,10))
 

		
INSERT INTO DBO.aBPC_STAGE ([LEDGER_ID],[COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],[A_TYPE],[CURRENCY],[A_FLAG],[PERIOD_NAME],[PERIOD_YEAR],[PERIOD_NUM],[SOURCE],[DATASRC],[CATEGORY],[TIME],[TIMEID],[SIGNEDDATA])


SELECT CAST([LEDGER_ID]AS NVARCHAR(20)),CAST(SEGMENT1+'_COM' AS NVARCHAR(20)) AS COMPANY,CAST(SEGMENT2+'_CCT' AS NVARCHAR(20)) AS COSTCENTRE,CAST(SEGMENT3+'_ACC' AS NVARCHAR(20)) AS ACCOUNT,CAST(SEGMENT4+'_ICO' AS NVARCHAR(20)) AS INTCO,CAST(SEGMENT5+'_PRV' AS NVARCHAR(20))AS PROVINCE,CAST(SEGMENT6+'_PRG' AS NVARCHAR(20))AS PROGRAM,CAST(SEGMENT7+'_PRD' AS NVARCHAR(20))AS PRODUCT,CAST(SEGMENT8+'_LOB' AS NVARCHAR(20))AS LOB,CAST(SEGMENT9+'_PAR' AS NVARCHAR(20)) AS PAR,CAST(SEGMENT10+'_PRJ' AS NVARCHAR(20))AS PROJECT,CAST(SEGMENT11+'_TRT' AS NVARCHAR(20)) AS TREATY,CAST(SEGMENT12+'_REI' AS NVARCHAR(20)) AS REINSURER,CAST(SEGMENT13+'_LYR' AS NVARCHAR(20)) AS LOSSYEAR,CAST(ACCOUNT_TYPE AS NVARCHAR(20))AS A_TYPE,CAST(CURRENCY_CODE AS NVARCHAR(20))AS CURRENCY,CAST(ACTUAL_FLAG AS NVARCHAR(20))AS A_FLAG,PERIOD_NAME,CAST(PERIOD_YEAR AS NVARCHAR(20)),CAST(PERIOD_NUM AS NVARCHAR(20)),                                                                                                                   
		'0' AS SOURCE,
		CAST('ORACLE' AS NVARCHAR(20)) AS DATASRC,
		CASE--DETERMINES CATEGORY VALURE
			WHEN ledger_id =190 THEN CAST('ACTUAL' AS NVARCHAR(20))
			ELSE CAST('ACTUAL_CON' AS NVARCHAR(20))
		END CATEGORY,
		CASE--ADDED IN CASE THIS TABLE IS LOADED USING BPC IMPORT SQL PACKAGE
			WHEN PERIOD_NUM =1 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JAN' 
			WHEN PERIOD_NUM =2 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.FEB' 
			WHEN PERIOD_NUM =3 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.MAR' 
			WHEN PERIOD_NUM =4 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.APR' 
			WHEN PERIOD_NUM =5 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.MAY' 
			WHEN PERIOD_NUM =6 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JUN' 
			WHEN PERIOD_NUM =7 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JUL' 
			WHEN PERIOD_NUM =8 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.AUG' 
			WHEN PERIOD_NUM =9 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.SEP' 
			WHEN PERIOD_NUM =10 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.OCT'
			WHEN PERIOD_NUM =11 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.NOV'
			WHEN PERIOD_NUM =12 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.DEC'
			ELSE ''
		END [TIME],
		CAST(PERIOD_YEAR AS NVARCHAR(20))+ 
		CASE --ADDED IN CASE DIRECT SQL LOAD PACKAGE IS USED
			WHEN LEN(CAST(PERIOD_NUM AS NVARCHAR(20))) = 1 THEN '0'+CAST(period_num AS NVARCHAR(20)) ELSE CAST(period_num AS NVARCHAR(20)) END +
		'00' AS TIMEID,
		CASE 
			WHEN CAST(account_type AS NVARCHAR(20)) IN ('A','L','O')  THEN (CAST(begin_balance_dr AS DECIMAL(25,10)) - CAST(begin_balance_cr AS DECIMAL(25,10))) + (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
			WHEN CAST(account_type AS NVARCHAR(20))IN ('R','E') THEN (CAST(period_net_dr AS DECIMAL(25,10))    - CAST(period_net_cr AS DECIMAL(25,10))) 
			ELSE (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
		END SIGNEDDATA		
		FROM openquery(PROD_LINK,'SELECT GLB.ledger_id, GLB.last_update_date, GLB.code_combination_id,
          GLB.begin_balance_dr, GLB.begin_balance_cr, GLB.period_net_dr, GLB.period_net_cr, GLB.period_name,
          GLB.period_num, GLB.period_year, GLB.currency_code, GLB.actual_flag, gcc.segment1, gcc.segment2,
          gcc.segment3, gcc.segment4, gcc.segment5, gcc.segment6, gcc.segment7, gcc.segment8, gcc.segment9,
          gcc.segment10, gcc.segment11, gcc.segment12, gcc.segment13, gcc.account_type
     FROM gl.gl_balances GLB, gl.gl_code_combinations gcc
     WHERE GLB.period_year >= 2010
     AND (GLB.BEGIN_BALANCE_CR <> 0 or GLB.BEGIN_BALANCE_DR <> 0 or GLB.PERIOD_NET_CR <> 0 or GLB.PERIOD_NET_DR <>0) 
     AND GLB.actual_flag = ''A''
     AND GLB.ledger_id IN
           (SELECT ledger_id
            FROM gl.gl_ledgers GL_LEDGERS1
            WHERE GL_LEDGERS1.ROWID IN
                   (SELECT GL_LEDGERS2.ROWID
                    FROM gl.gl_ledgers GL_LEDGERS2
                    WHERE name = ''The Co-operators Group of Co''
                    UNION
                    SELECT GL_LEDGERS3.ROWID
                    FROM gl.gl_ledgers GL_LEDGERS3
                    WHERE name = ''CGC Consolidated''))
     AND (GLB.currency_code = ''CAD'' OR (GLB.currency_code = ''STAT''
     AND gcc.segment3 IN (SELECT account_id FROM xxcgc_rpt.xxcgc_rpt_z00000_acct_flath)))
     AND GLB.code_combination_id = gcc.code_combination_id
     AND gcc.segment1 <> ''T''
     AND gcc.segment2 <> ''T''
     AND gcc.segment3 <> ''T''
     AND gcc.segment4 <> ''T''
     AND gcc.segment5 <> ''T''
     AND gcc.segment6 <> ''T''
     AND gcc.segment7 <> ''T''
     AND gcc.segment8 <> ''T''
     AND gcc.segment9 <> ''T''
     AND gcc.segment10 <> ''T''
     AND gcc.segment11 <> ''T''
     AND gcc.segment12 <> ''T''
     AND gcc.segment13 <> ''T''')
	--	FROM openquery(PROD_LINK,'select * from xxcgc_rpt.xxcgc_shr_acctsegments_v')
		/* and a.last_update_date > to_date('"+ @[User::Last_run_date]+ "', 'yyyy-mm-dd hh24:mi:ss')*/ -- this was part of the above where clause
		WHERE CAST(period_year AS NVARCHAR(20)) >= (SELECT Value FROM tblDefaults WHERE KeyID = 'PERIOD_YEAR_TO_LOAD')--	THIS ALLOWS THE USER TO INPUT THE YEAR FOR WHICH TO PULL DATA
			AND (account_type IN ('A','L','O') AND (CAST(begin_balance_dr AS DECIMAL(25,10)) - CAST(begin_balance_cr AS DECIMAL(25,10))) + (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10))) <>0)
			OR (account_type IN ('E','R') AND (CAST(period_net_dr AS DECIMAL(25,10))    - CAST(period_net_cr AS DECIMAL(25,10))) <> 0)
			OR (account_type NOT IN ('A','L','O','E','R') AND (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))<> 0)
			AND SEGMENT6  <> 'T_PRG'
			AND SEGMENT5  <> 'T_PRV'
			AND SEGMENT11 <> 'T_TRT'
			AND SEGMENT2  <> 'T_CCT'
			AND SEGMENT7  <> 'T_PRD'
			AND SEGMENT12 <> 'T_REI'
			AND SEGMENT4  <> 'T_ICO'
			AND SEGMENT10 <> 'T_PRJ'
			AND SEGMENT13 <> 'T_LYR'
			AND SEGMENT8  <> 'T_LOB'
			AND SEGMENT9  <> 'T_PAR'
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------BEGIN INTEGRITY CHECK-----------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------Tests for records in Oracle that are not in BPC-------------------------------------------
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		SIGNEDDATA,'in Oracle but not BPC' AS STATUS		
FROM(
		SELECT a.Company,a.Account,a.CostCentre,a.Intco,a.Lob,a.Lossyear,a.Par,a.Product,a.Program,a.Project,a.Province,
		a.Reinsurer,a.Treaty,a.TIMEID,a.CATEGORY,a.SIGNEDDATA
		FROM dbo.TMPBPC_STAGE a--INLINE TABLE FUNCTION PERFORMS ETL

		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM TBLFACTMAIN
				WHERE CATEGORY in ('ACTUAL','ACTUAL_CON'))
	)W
UNION
----------------------------Tests for records in Oracle that are not in BPC-------------------------------------------
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		SIGNEDDATA,'in BPC but not Oracle' AS STATUS
FROM(
		SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		CATEGORY,SIGNEDDATA
		FROM TBLFACTMAIN
		WHERE CATEGORY in ('ACTUAL','ACTUAL_CON')
		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM dbo.TMPBPC_STAGE a--INLINE TABLE FUNCTION PERFORMS ETL
				)
		)x
--order by STATUS,DIMENSION,MEMBER
END
ELSE
BEGIN
TRUNCATE TABLE dbo.TMPBPC_STAGE

INSERT INTO dbo.TMPBPC_STAGE ([LEDGER_ID],[COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],[A_TYPE],[CURRENCY],[A_FLAG],[PERIOD_NAME],[PERIOD_YEAR],[PERIOD_NUM],[SOURCE],[DATASRC],[CATEGORY],[TIME],[TIMEID],[SIGNEDDATA])


SELECT CAST([LEDGER_ID]AS NVARCHAR(20)),CAST(SEGMENT1+'_COM' AS NVARCHAR(20)) AS COMPANY,CAST(SEGMENT2+'_CCT' AS NVARCHAR(20)) AS COSTCENTRE,CAST(SEGMENT3+'_ACC' AS NVARCHAR(20)) AS ACCOUNT,CAST(SEGMENT4+'_ICO' AS NVARCHAR(20)) AS INTCO,CAST(SEGMENT5+'_PRV' AS NVARCHAR(20))AS PROVINCE,CAST(SEGMENT6+'_PRG' AS NVARCHAR(20))AS PROGRAM,CAST(SEGMENT7+'_PRD' AS NVARCHAR(20))AS PRODUCT,CAST(SEGMENT8+'_LOB' AS NVARCHAR(20))AS LOB,CAST(SEGMENT9+'_PAR' AS NVARCHAR(20)) AS PAR,CAST(SEGMENT10+'_PRJ' AS NVARCHAR(20))AS PROJECT,CAST(SEGMENT11+'_TRT' AS NVARCHAR(20)) AS TREATY,CAST(SEGMENT12+'_REI' AS NVARCHAR(20)) AS REINSURER,CAST(SEGMENT13+'_LYR' AS NVARCHAR(20)) AS LOSSYEAR,CAST(ACCOUNT_TYPE AS NVARCHAR(20))AS A_TYPE,CAST(CURRENCY_CODE AS NVARCHAR(20))AS CURRENCY,CAST(ACTUAL_FLAG AS NVARCHAR(20))AS A_FLAG,PERIOD_NAME,CAST(PERIOD_YEAR AS NVARCHAR(20)),CAST(PERIOD_NUM AS NVARCHAR(20)),                                                                                                                   
		'0' AS SOURCE,
		CAST('ORACLE' AS NVARCHAR(20)) AS DATASRC,
		CASE--DETERMINES CATEGORY VALURE
			WHEN ledger_id =190 THEN CAST('ACTUAL' AS NVARCHAR(20))
			ELSE CAST('ACTUAL_CON' AS NVARCHAR(20))
		END CATEGORY,
		CASE--ADDED IN CASE THIS TABLE IS LOADED USING BPC IMPORT SQL PACKAGE
			WHEN PERIOD_NUM =1 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JAN' 
			WHEN PERIOD_NUM =2 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.FEB' 
			WHEN PERIOD_NUM =3 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.MAR' 
			WHEN PERIOD_NUM =4 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.APR' 
			WHEN PERIOD_NUM =5 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.MAY' 
			WHEN PERIOD_NUM =6 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JUN' 
			WHEN PERIOD_NUM =7 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JUL' 
			WHEN PERIOD_NUM =8 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.AUG' 
			WHEN PERIOD_NUM =9 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.SEP' 
			WHEN PERIOD_NUM =10 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.OCT'
			WHEN PERIOD_NUM =11 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.NOV'
			WHEN PERIOD_NUM =12 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.DEC'
			ELSE ''
		END [TIME],
		CAST(PERIOD_YEAR AS NVARCHAR(20))+ 
		CASE --ADDED IN CASE DIRECT SQL LOAD PACKAGE IS USED
			WHEN LEN(CAST(PERIOD_NUM AS NVARCHAR(20))) = 1 THEN '0'+CAST(period_num AS NVARCHAR(20)) ELSE CAST(period_num AS NVARCHAR(20)) END +
		'00' AS TIMEID,
		CASE 
			WHEN CAST(account_type AS NVARCHAR(20)) IN ('A','L','O')  THEN (CAST(begin_balance_dr AS DECIMAL(25,10)) - CAST(begin_balance_cr AS DECIMAL(25,10))) + (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
			WHEN CAST(account_type AS NVARCHAR(20))IN ('R','E') THEN (CAST(period_net_dr AS DECIMAL(25,10))    - CAST(period_net_cr AS DECIMAL(25,10))) 
			ELSE (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
		END SIGNEDDATA		
		FROM openquery(PROD_LINK,'SELECT GLB.ledger_id, GLB.last_update_date, GLB.code_combination_id,
          GLB.begin_balance_dr, GLB.begin_balance_cr, GLB.period_net_dr, GLB.period_net_cr, GLB.period_name,
          GLB.period_num, GLB.period_year, GLB.currency_code, GLB.actual_flag, gcc.segment1, gcc.segment2,
          gcc.segment3, gcc.segment4, gcc.segment5, gcc.segment6, gcc.segment7, gcc.segment8, gcc.segment9,
          gcc.segment10, gcc.segment11, gcc.segment12, gcc.segment13, gcc.account_type
     FROM gl.gl_balances GLB, gl.gl_code_combinations gcc
     WHERE GLB.period_year >= 2010
     AND (GLB.BEGIN_BALANCE_CR <> 0 or GLB.BEGIN_BALANCE_DR <> 0 or GLB.PERIOD_NET_CR <> 0 or GLB.PERIOD_NET_DR <>0) 
     AND GLB.actual_flag = ''A''
     AND GLB.ledger_id IN
           (SELECT ledger_id
            FROM gl.gl_ledgers GL_LEDGERS1
            WHERE GL_LEDGERS1.ROWID IN
                   (SELECT GL_LEDGERS2.ROWID
                    FROM gl.gl_ledgers GL_LEDGERS2
                    WHERE name = ''The Co-operators Group of Co''
                    UNION
                    SELECT GL_LEDGERS3.ROWID
                    FROM gl.gl_ledgers GL_LEDGERS3
                    WHERE name = ''CGC Consolidated''))
     AND (GLB.currency_code = ''CAD'' OR (GLB.currency_code = ''STAT''
     AND gcc.segment3 IN (SELECT account_id FROM xxcgc_rpt.xxcgc_rpt_z00000_acct_flath)))
     AND GLB.code_combination_id = gcc.code_combination_id
     AND gcc.segment1 <> ''T''
     AND gcc.segment2 <> ''T''
     AND gcc.segment3 <> ''T''
     AND gcc.segment4 <> ''T''
     AND gcc.segment5 <> ''T''
     AND gcc.segment6 <> ''T''
     AND gcc.segment7 <> ''T''
     AND gcc.segment8 <> ''T''
     AND gcc.segment9 <> ''T''
     AND gcc.segment10 <> ''T''
     AND gcc.segment11 <> ''T''
     AND gcc.segment12 <> ''T''
     AND gcc.segment13 <> ''T''')
	--	FROM openquery(PROD_LINK,'select * from xxcgc_rpt.xxcgc_shr_acctsegments_v')
		/* and a.last_update_date > to_date('"+ @[User::Last_run_date]+ "', 'yyyy-mm-dd hh24:mi:ss')*/ -- this was part of the above where clause
		WHERE CAST(period_year AS NVARCHAR(20)) >= (SELECT Value FROM tblDefaults WHERE KeyID = 'PERIOD_YEAR_TO_LOAD')--	THIS ALLOWS THE USER TO INPUT THE YEAR FOR WHICH TO PULL DATA
			AND (account_type IN ('A','L','O') AND (CAST(begin_balance_dr AS DECIMAL(25,10)) - CAST(begin_balance_cr AS DECIMAL(25,10))) + (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10))) <>0)
			OR (account_type IN ('E','R') AND (CAST(period_net_dr AS DECIMAL(25,10))    - CAST(period_net_cr AS DECIMAL(25,10))) <> 0)
			OR (account_type NOT IN ('A','L','O','E','R') AND (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))<> 0)
			AND SEGMENT6  <> 'T_PRG'
			AND SEGMENT5  <> 'T_PRV'
			AND SEGMENT11 <> 'T_TRT'
			AND SEGMENT2  <> 'T_CCT'
			AND SEGMENT7  <> 'T_PRD'
			AND SEGMENT12 <> 'T_REI'
			AND SEGMENT4  <> 'T_ICO'
			AND SEGMENT10 <> 'T_PRJ'
			AND SEGMENT13 <> 'T_LYR'
			AND SEGMENT8  <> 'T_LOB'
			AND SEGMENT9  <> 'T_PAR'
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------BEGIN INTEGRITY CHECK-----------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------Tests for records in Oracle that are not in BPC-------------------------------------------
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		SIGNEDDATA,'in Oracle but not BPC' AS STATUS		
FROM(
		SELECT a.Company,a.Account,a.CostCentre,a.Intco,a.Lob,a.Lossyear,a.Par,a.Product,a.Program,a.Project,a.Province,
		a.Reinsurer,a.Treaty,a.TIMEID,a.CATEGORY,a.SIGNEDDATA
		FROM dbo.TMPBPC_STAGE a--INLINE TABLE FUNCTION PERFORMS ETL
		
		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY,SIGNEDDATA
				FROM TBLFACTMAIN
				WHERE LEFT(TIMEID,4) >= (SELECT VALUE FROM tblDefaults WHERE KeyID = 'PERIOD_YEAR_TO_LOAD')
				and CATEGORY in ('ACTUAL','ACTUAL_CON'))
	)W
UNION
----------------------------Tests for records in Oracle that are not in BPC-------------------------------------------
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		SIGNEDDATA,'in BPC but not Oracle' AS STATUS
FROM(
		SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY,SIGNEDDATA
		FROM TBLFACTMAIN
		WHERE LEFT(TIMEID,4) >= (SELECT VALUE FROM tblDefaults WHERE KeyID = 'PERIOD_YEAR_TO_LOAD')
		and CATEGORY in ('ACTUAL','ACTUAL_CON')
		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY,SIGNEDDATA
				FROM dbo.TMPBPC_STAGE a)
		)x
END