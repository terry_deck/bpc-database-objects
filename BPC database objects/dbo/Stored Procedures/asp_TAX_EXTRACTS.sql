﻿


CREATE PROCEDURE [dbo].[asp_TAX_EXTRACTS] (@company nvarchar(7), @rundate nvarchar(6))

AS
SET NOCOUNT ON;

BEGIN
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aTAX_STAGE]') AND type in (N'U'))
DROP TABLE [dbo].[aTAX_STAGE]

CREATE TABLE [dbo].[aTAX_STAGE](
	[TAXREC] [nvarchar](100) NULL,
	[ACCOUNT] [nvarchar](14) NULL,
	[COMPANY] [nvarchar](7) NULL
) ON [PRIMARY]

END

BEGIN

INSERT INTO dbo.aTAX_STAGE ([TAXREC],[ACCOUNT],[COMPANY])
select substring(q.account,1,8)+
'{A'+SUBSTRING(@rundate,3,4)+'YTD{'+
SUBSTRING(q.COMPANY,1,3)+'_E{CAD{'+
cast(cast(sum(q.signeddata) AS money) as nvarchar) taxrec,
q.account,
q.company
FROM
 (Select ACCOUNT,COMPANY, SIGNEDDATA, TIMEID
  from tblFactMain a, mbrAccount b
  where COMPANY = (@company)
  and substring(TIMEID,1,6) = @rundate
  and a.ACCOUNT = b.ID
  and (a.ACCOUNT like '1%' or a.ACCOUNT like '2%' or a.ACCOUNT like '3%' or
       a.ACCOUNT like '4%' or a.ACCOUNT like '5%' or a.ACCOUNT like '6%' or
       a.ACCOUNT like '7%' or a.ACCOUNT like '8%' or a.ACCOUNT like '9%')
  and a.ACCOUNT not like '%_ACC_PER'       
  and a.ACCOUNT not like '%ST_%'
  and a.ACCOUNT not like '%_MAIN'
  and b.ACCTYPE in('LEQ','AST')
  and A.CATEGORY in ('ACTUAL','ACTUAL_POOL')
  union all
 Select ACCOUNT,COMPANY, SIGNEDDATA,TIMEID
 from tblFAC2Main a, mbrAccount b
 where COMPANY = (@company)
 and substring(TIMEID,1,6) = @rundate
 and a.ACCOUNT = b.ID
 and (a.ACCOUNT like '1%' or a.ACCOUNT like '2%' or a.ACCOUNT like '3%' or
      a.ACCOUNT like '4%' or a.ACCOUNT like '5%' or a.ACCOUNT like '6%' or
      a.ACCOUNT like '7%' or a.ACCOUNT like '8%' or a.ACCOUNT like '9%')
 and a.ACCOUNT not like '%_ACC_PER'  
 and a.ACCOUNT not like '%ST_%'
 and a.ACCOUNT not like '%_MAIN'
 and b.ACCTYPE in('LEQ','AST')
 and A.CATEGORY in ('ACTUAL','ACTUAL_POOL')
 union all
 Select ACCOUNT,COMPANY, SIGNEDDATA,TIMEID
 from tblFACTWBMain a, mbrAccount b
 where COMPANY = (@company)
 and substring(TIMEID,1,6) = @rundate
 and a.ACCOUNT = b.ID
 and (a.ACCOUNT like '1%' or a.ACCOUNT like '2%' or a.ACCOUNT like '3%' or
      a.ACCOUNT like '4%' or a.ACCOUNT like '5%' or a.ACCOUNT like '6%' or
      a.ACCOUNT like '7%' or a.ACCOUNT like '8%' or a.ACCOUNT like '9%')
 and a.ACCOUNT not like '%_ACC_PER'
 and a.ACCOUNT not like '%ST_%'
 and a.ACCOUNT not like '%_MAIN'
 and b.ACCTYPE in('LEQ','AST')
 and A.CATEGORY in ('ACTUAL','ACTUAL_POOL'))  as Q
 group by ACCOUNT,COMPANY, TIMEID
 having SUM(signeddata) <> 0
 order by ACCOUNT,COMPANY,TIMEID

END
     
BEGIN
INSERT INTO DBO.aTAX_STAGE ([TAXREC],[ACCOUNT],[COMPANY])

select substring(z.account,1,8)+
'{A'+SUBSTRING(@rundate,3,4)+'YTD{'+
SUBSTRING(z.COMPANY,1,3)+'_E{CAD{'+
cast(cast(sum(z.signeddata) AS money) as nvarchar) taxrec,
z.account,
z.company
FROM
 (Select ACCOUNT,COMPANY, SIGNEDDATA, TIMEID
  from tblFactMain a, mbrAccount b
  where COMPANY = (@company)
  and TIMEID like substring(@rundate, 1, 4)+'%'
  and substring(TIMEID,1,6) <= @rundate
  and a.ACCOUNT = b.ID
  and (a.ACCOUNT like '1%' or a.ACCOUNT like '2%' or a.ACCOUNT like '3%' or
       a.ACCOUNT like '4%' or a.ACCOUNT like '5%' or a.ACCOUNT like '6%' or
       a.ACCOUNT like '7%' or a.ACCOUNT like '8%' or a.ACCOUNT like '9%')
  and a.ACCOUNT not like '%_ACC_PER'  
  and a.ACCOUNT not like '%ST_%'
  and a.ACCOUNT not like '%_MAIN'
  and b.ACCTYPE in('EXP','INC')
  and A.CATEGORY in ('ACTUAL','ACTUAL_POOL')
  union all
 Select ACCOUNT,COMPANY, SIGNEDDATA,TIMEID
 from tblFAC2Main a, mbrAccount b
 where COMPANY = (@company)
 and TIMEID like substring(@rundate, 1, 4)+'%'
 and substring(TIMEID,1,6) <= @rundate
 and a.ACCOUNT = b.ID
 and (a.ACCOUNT like '1%' or a.ACCOUNT like '2%' or a.ACCOUNT like '3%' or
      a.ACCOUNT like '4%' or a.ACCOUNT like '5%' or a.ACCOUNT like '6%' or
      a.ACCOUNT like '7%' or a.ACCOUNT like '8%' or a.ACCOUNT like '9%')
 and a.ACCOUNT not like '%_ACC_PER'  
 and a.ACCOUNT not like '%ST_%'
 and a.ACCOUNT not like '%_MAIN'
 and b.ACCTYPE in('EXP','INC')
 and A.CATEGORY in ('ACTUAL','ACTUAL_POOL')
 union all
 Select ACCOUNT,COMPANY, SIGNEDDATA,TIMEID
 from tblFACTWBMain a, mbrAccount b
 where COMPANY = (@company)
 and TIMEID like substring(@rundate, 1, 4)+'%'
 and substring(TIMEID,1,6) <= @rundate
 and a.ACCOUNT = b.ID
 and (a.ACCOUNT like '1%' or a.ACCOUNT like '2%' or a.ACCOUNT like '3%' or
      a.ACCOUNT like '4%' or a.ACCOUNT like '5%' or a.ACCOUNT like '6%' or
      a.ACCOUNT like '7%' or a.ACCOUNT like '8%' or a.ACCOUNT like '9%')
 and a.ACCOUNT not like '%_ACC_PER'
 and a.ACCOUNT not like '%ST_%'
 and a.ACCOUNT not like '%_MAIN'
 and b.ACCTYPE in('EXP','INC')
 and A.CATEGORY in ('ACTUAL','ACTUAL_POOL')) as Z
 group by ACCOUNT,COMPANY
 having SUM(signeddata) <> 0
 order by ACCOUNT,COMPANY

END   

