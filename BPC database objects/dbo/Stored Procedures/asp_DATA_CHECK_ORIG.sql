﻿
CREATE procedure [dbo].[asp_DATA_CHECK_ORIG]

AS

--------------------------------BEGIN DATA CHECK-----------------------------------------------------

IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES	 WHERE TABLE_NAME = 'TMPBPC_STAGE')


BEGIN
CREATE TABLE dbo.TMPBPC_STAGE 

--CREATE TEMORY TABLE FOR BUILDING THE REPORT
(
[LEDGER_ID]   NVARCHAR(20),
[COMPANY]     NVARCHAR(20),
[COSTCENTRE]  NVARCHAR(20),
[ACCOUNT]     NVARCHAR(20),
[INTCO]       NVARCHAR(20),
[PROVINCE]    NVARCHAR(20),
[PROGRAM]     NVARCHAR(20),
[PRODUCT]     NVARCHAR(20),
[LOB]         NVARCHAR(20),
[PAR]         NVARCHAR(20),
[PROJECT]     NVARCHAR(20),
[TREATY]      NVARCHAR(20),
[REINSURER]   NVARCHAR(20),
[LOSSYEAR]    NVARCHAR(20),
[A_TYPE]      NVARCHAR(20),
[CURRENCY]    NVARCHAR(20),
[A_FLAG]      NVARCHAR(20),
[PERIOD_NAME] NVARCHAR(20),
[PERIOD_YEAR] NVARCHAR(20),
[PERIOD_NUM]  NVARCHAR(20),
[SOURCE]      NVARCHAR(20),
[DATASRC]     NVARCHAR(20),
[CATEGORY]    NVARCHAR(20),
[TIME]        NVARCHAR(20),
[TIMEID]      NVARCHAR(20),
[SIGNEDDATA]  DECIMAL(25,10))
 

INSERT INTO dbo.TMPBPC_STAGE ([LEDGER_ID],[COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],[A_TYPE],[CURRENCY],[A_FLAG],[PERIOD_NAME],[PERIOD_YEAR],[PERIOD_NUM],[SOURCE],[DATASRC],[CATEGORY],[TIME],[TIMEID],[SIGNEDDATA])


SELECT CAST([LEDGER_ID]AS NVARCHAR(20)),CAST(SEGMENT1+'_COM' AS NVARCHAR(20)) AS COMPANY,CAST(SEGMENT2+'_CCT' AS NVARCHAR(20)) AS COSTCENTRE,CAST(SEGMENT3+'_ACC' AS NVARCHAR(20)) AS ACCOUNT,CAST(SEGMENT4+'_ICO' AS NVARCHAR(20)) AS INTCO,CAST(SEGMENT5+'_PRV' AS NVARCHAR(20))AS PROVINCE,CAST(SEGMENT6+'_PRG' AS NVARCHAR(20))AS PROGRAM,CAST(SEGMENT7+'_PRD' AS NVARCHAR(20))AS PRODUCT,CAST(SEGMENT8+'_LOB' AS NVARCHAR(20))AS LOB,CAST(SEGMENT9+'_PAR' AS NVARCHAR(20)) AS PAR,CAST(SEGMENT10+'_PRJ' AS NVARCHAR(20))AS PROJECT,CAST(SEGMENT11+'_TRT' AS NVARCHAR(20)) AS TREATY,CAST(SEGMENT12+'_REI' AS NVARCHAR(20)) AS REINSURER,CAST(SEGMENT13+'_LYR' AS NVARCHAR(20)) AS LOSSYEAR,CAST(ACCOUNT_TYPE AS NVARCHAR(20))AS A_TYPE,CAST(CURRENCY_CODE AS NVARCHAR(20))AS CURRENCY,CAST(ACTUAL_FLAG AS NVARCHAR(20))AS A_FLAG,PERIOD_NAME,CAST(PERIOD_YEAR AS NVARCHAR(20)),CAST(PERIOD_NUM AS NVARCHAR(20)),                                                                                                                   
		'0' AS SOURCE,
		CAST('ORACLE' AS NVARCHAR(20)) AS DATASRC,
		CASE--DETERMINES CATEGORY VALURE
			WHEN ledger_id =190 THEN CAST('ACTUAL' AS NVARCHAR(20))
			ELSE CAST('ACTUAL_CON' AS NVARCHAR(20))
		END CATEGORY,
		CASE--ADDED IN CASE THIS TABLE IS LOADED USING BPC IMPORT SQL PACKAGE
			WHEN PERIOD_NUM =1 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JAN' 
			WHEN PERIOD_NUM =2 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.FEB' 
			WHEN PERIOD_NUM =3 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.MAR' 
			WHEN PERIOD_NUM =4 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.APR' 
			WHEN PERIOD_NUM =5 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.MAY' 
			WHEN PERIOD_NUM =6 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JUN' 
			WHEN PERIOD_NUM =7 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JUL' 
			WHEN PERIOD_NUM =8 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.AUG' 
			WHEN PERIOD_NUM =9 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.SEP' 
			WHEN PERIOD_NUM =10 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.OCT'
			WHEN PERIOD_NUM =11 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.NOV'
			WHEN PERIOD_NUM =12 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.DEC'
			ELSE ''
		END [TIME],
		CAST(PERIOD_YEAR AS NVARCHAR(20))+ 
		CASE --ADDED IN CASE DIRECT SQL LOAD PACKAGE IS USED
			WHEN LEN(CAST(PERIOD_NUM AS NVARCHAR(20))) = 1 THEN '0'+CAST(period_num AS NVARCHAR(20)) ELSE CAST(period_num AS NVARCHAR(20)) END +
		'00' AS TIMEID,
		CASE 
			WHEN CAST(account_type AS NVARCHAR(20)) IN ('A','L','O')  THEN (CAST(begin_balance_dr AS DECIMAL(25,10)) - CAST(begin_balance_cr AS DECIMAL(25,10))) + (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
			WHEN CAST(account_type AS NVARCHAR(20))IN ('R','E') THEN (CAST(period_net_dr AS DECIMAL(25,10))    - CAST(period_net_cr AS DECIMAL(25,10))) 
			ELSE (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
		END SIGNEDDATA		
	--	FROM openquery(PROD_LINK,'select a.ledger_id, a.last_update_date, a.code_combination_id, a.begin_balance_dr, a.begin_balance_cr, a.period_net_dr,a.period_net_cr, a.period_name, a.period_num, a.period_year, a.currency_code, a.actual_flag, b.segment1, b.segment2, b.segment3, b.segment4, b.segment5, b.segment6, b.segment7, b.segment8, b.segment9, b.segment10, b.segment11, b.segment12, b.segment13, b.account_type  from gl.gl_balances a , gl.gl_code_combinations b  where a.period_year >= 2010 and a.currency_code = ''CAD''
	--					and a.actual_flag =''A''and a.ledger_id in (select ledger_id from gl.gl_ledgers where name  in (''The Co-operators Group of Co'',''CGC Consolidated''))  and A.CODE_COMBINATION_ID = b.code_combination_id
	--					and b.segment1 <>''T'' and b.segment2<> ''T'' and b.segment3 <> ''T''
	--					and b.segment4 <> ''T'' and b.segment5 <>''T'' and b.segment6<> ''T''
	--					and b.segment7 <> ''T'' and b.segment8   <> ''T'' and b.segment9 <>''T''
	--					and b.segment10 <> ''T'' and b.segment11 <> ''T'' and b.segment12<> ''T''
	--					and b.segment13 <>''T''')
		FROM openquery(PROD_LINK,'select * from xxcgc_rpt.xxcgc_shr_acctsegments_v')
		/* and a.last_update_date > to_date('"+ @[User::Last_run_date]+ "', 'yyyy-mm-dd hh24:mi:ss')*/ -- this was part of the above where clause
		WHERE CAST(period_year AS NVARCHAR(20)) >= (SELECT Value FROM tblDefaults WHERE KeyID = 'PERIOD_YEAR_TO_LOAD')--	THIS ALLOWS THE USER TO INPUT THE YEAR FOR WHICH TO PULL DATA
			AND (account_type IN ('A','L','O') AND (CAST(begin_balance_dr AS DECIMAL(25,10)) - CAST(begin_balance_cr AS DECIMAL(25,10))) + (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10))) <>0)
			OR (account_type IN ('E','R') AND (CAST(period_net_dr AS DECIMAL(25,10))    - CAST(period_net_cr AS DECIMAL(25,10))) <> 0)
			OR (account_type NOT IN ('A','L','O','E','R') AND (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))<> 0)
			AND SEGMENT6  <> 'T_PRG'
			AND SEGMENT5  <> 'T_PRV'
			AND SEGMENT11 <> 'T_TRT'
			AND SEGMENT2  <> 'T_CCT'
			AND SEGMENT7  <> 'T_PRD'
			AND SEGMENT12 <> 'T_REI'
			AND SEGMENT4  <> 'T_ICO'
			AND SEGMENT10 <> 'T_PRJ'
			AND SEGMENT13 <> 'T_LYR'
			AND SEGMENT8  <> 'T_LOB'
			AND SEGMENT9  <> 'T_PAR'
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------BEGIN INTEGRITY CHECK-----------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------Tests for records in Oracle that are not in BPC-------------------------------------------
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		SIGNEDDATA,'in Oracle but not BPC' AS STATUS		
FROM(
		SELECT a.Company,a.Account,a.CostCentre,a.Intco,a.Lob,a.Lossyear,a.Par,a.Product,a.Program,a.Project,a.Province,
		a.Reinsurer,a.Treaty,a.TIMEID,a.CATEGORY,a.SIGNEDDATA
		FROM dbo.TMPBPC_STAGE a--INLINE TABLE FUNCTION PERFORMS ETL

		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM TBLFACTMAIN
				WHERE CATEGORY in ('ACTUAL','ACTUAL_CON'))
	)W
UNION
----------------------------Tests for records in Oracle that are not in BPC-------------------------------------------
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		SIGNEDDATA,'in BPC but not Oracle' AS STATUS
FROM(
		SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		CATEGORY,SIGNEDDATA
		FROM TBLFACTMAIN
		WHERE CATEGORY in ('ACTUAL','ACTUAL_CON')
		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
				CATEGORY,SIGNEDDATA
				FROM dbo.TMPBPC_STAGE a--INLINE TABLE FUNCTION PERFORMS ETL
				)
		)x
--order by STATUS,DIMENSION,MEMBER
END
ELSE
BEGIN
TRUNCATE TABLE dbo.TMPBPC_STAGE

INSERT INTO dbo.TMPBPC_STAGE ([LEDGER_ID],[COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],[A_TYPE],[CURRENCY],[A_FLAG],[PERIOD_NAME],[PERIOD_YEAR],[PERIOD_NUM],[SOURCE],[DATASRC],[CATEGORY],[TIME],[TIMEID],[SIGNEDDATA])


SELECT [LEDGER_ID],CAST(SEGMENT1+'_COM' AS NVARCHAR(20)) AS COMPANY,CAST(SEGMENT2+'_CCT' AS NVARCHAR(20)) AS COSTCENTRE,CAST(SEGMENT3+'_ACC' AS NVARCHAR(20)) AS ACCOUNT,CAST(SEGMENT4+'_ICO' AS NVARCHAR(20)) AS INTCO,CAST(SEGMENT5+'_PRV' AS NVARCHAR(20))AS PROVINCE,CAST(SEGMENT6+'_PRG' AS NVARCHAR(20))AS PROGRAM,CAST(SEGMENT7+'_PRD' AS NVARCHAR(20))AS PRODUCT,CAST(SEGMENT8+'_LOB' AS NVARCHAR(20))AS LOB,CAST(SEGMENT9+'_PAR' AS NVARCHAR(20)) AS PAR,CAST(SEGMENT10+'_PRJ' AS NVARCHAR(20))AS PROJECT,CAST(SEGMENT11+'_TRT' AS NVARCHAR(20)) AS TREATY,CAST(SEGMENT12+'_REI' AS NVARCHAR(20)) AS REINSURER,CAST(SEGMENT13+'_LYR' AS NVARCHAR(20)) AS LOSSYEAR,ACCOUNT_TYPE AS A_TYPE,CURRENCY_CODE AS CURRENCY,ACTUAL_FLAG AS A_FLAG,PERIOD_NAME,CAST(PERIOD_YEAR AS NVARCHAR(20))AS PERIOD_YEAR,CAST(PERIOD_NUM AS NVARCHAR(20))AS PERIOD_NUM,                                                                                                                   
		'0' AS SOURCE,
		CAST('ORACLE' AS NVARCHAR(20)) AS DATASRC,
		CASE--DETERMINES CATEGORY VALURE
			WHEN ledger_id =190 THEN CAST('ACTUAL' AS NVARCHAR(20))
			ELSE CAST('ACTUAL_CON' AS NVARCHAR(20))
		END CATEGORY,
		CASE--ADDED IN CASE THIS TABLE IS LOADED USING BPC IMPORT SQL PACKAGE
			WHEN PERIOD_NUM =1 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JAN' 
			WHEN PERIOD_NUM =2 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.FEB' 
			WHEN PERIOD_NUM =3 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.MAR' 
			WHEN PERIOD_NUM =4 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.APR' 
			WHEN PERIOD_NUM =5 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.MAY' 
			WHEN PERIOD_NUM =6 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JUN' 
			WHEN PERIOD_NUM =7 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.JUL' 
			WHEN PERIOD_NUM =8 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.AUG' 
			WHEN PERIOD_NUM =9 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.SEP' 
			WHEN PERIOD_NUM =10 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.OCT'
			WHEN PERIOD_NUM =11 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.NOV'
			WHEN PERIOD_NUM =12 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+'.DEC'
			ELSE ''
		END [TIME],
		CAST(PERIOD_YEAR AS NVARCHAR(20))+ 
		CASE --ADDED IN CASE DIRECT SQL LOAD PACKAGE IS USED
			WHEN LEN(CAST(PERIOD_NUM AS NVARCHAR(20))) = 1 THEN '0'+CAST(period_num AS NVARCHAR(20)) ELSE CAST(period_num AS NVARCHAR(20)) END +
		'00' AS TIMEID,
		CASE 
			WHEN account_type IN ('A','L','O')  THEN (CAST(begin_balance_dr AS DECIMAL(25,10)) - CAST(begin_balance_cr AS DECIMAL(25,10))) + (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
			WHEN account_type IN ('R','E') THEN (CAST(period_net_dr AS DECIMAL(25,10))    - CAST(period_net_cr AS DECIMAL(25,10))) 
			ELSE (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
		END SIGNEDDATA

	--	FROM openquery(PROD_LINK,'select a.ledger_id, a.last_update_date, a.code_combination_id, a.begin_balance_dr, a.begin_balance_cr, a.period_net_dr,a.period_net_cr, a.period_name, a.period_num, a.period_year, a.currency_code, a.actual_flag, b.segment1, b.segment2, b.segment3, b.segment4, b.segment5, b.segment6, b.segment7, b.segment8, b.segment9, b.segment10, b.segment11, b.segment12, b.segment13, b.account_type  from gl.gl_balances a , gl.gl_code_combinations b  where a.period_year >= 2010 and a.currency_code = ''CAD''
	--					and a.actual_flag =''A''and a.ledger_id in (select ledger_id from gl.gl_ledgers where name  in (''The Co-operators Group of Co'',''CGC Consolidated''))  and A.CODE_COMBINATION_ID = b.code_combination_id
	--					and b.segment1 <>''T'' and b.segment2<> ''T'' and b.segment3 <> ''T''
	--					and b.segment4 <> ''T'' and b.segment5 <>''T'' and b.segment6<> ''T''
	--					and b.segment7 <> ''T'' and b.segment8   <> ''T'' and b.segment9 <>''T''
	--					and b.segment10 <> ''T'' and b.segment11 <> ''T'' and b.segment12<> ''T''
	--					and b.segment13 <>''T''')
		FROM openquery(PROD_LINK,'select * from xxcgc_rpt.xxcgc_shr_acctsegments_v')
		WHERE CAST(period_year AS NVARCHAR(20)) >= (SELECT Value FROM tblDefaults WHERE KeyID = 'PERIOD_YEAR_TO_LOAD')--	THIS ALLOWS THE USER TO INPUT THE YEAR FOR WHICH TO PULL DATA
		AND (account_type IN ('A','L','O') AND (CAST(begin_balance_dr AS DECIMAL(25,10)) - CAST(begin_balance_cr AS DECIMAL(25,10))) + (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10))) <>0)
		OR (account_type IN ('E','R') AND (CAST(period_net_dr AS DECIMAL(25,10))    - CAST(period_net_cr AS DECIMAL(25,10))) <> 0)
		OR (account_type NOT IN ('A','L','O','E','R') AND (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))<> 0)
			AND SEGMENT6  <> 'T_PRG'
			AND SEGMENT5  <> 'T_PRV'
			AND SEGMENT11 <> 'T_TRT'
			AND SEGMENT2  <> 'T_CCT'
			AND SEGMENT7  <> 'T_PRD'
			AND SEGMENT12 <> 'T_REI'
			AND SEGMENT4  <> 'T_ICO'
			AND SEGMENT10 <> 'T_PRJ'
			AND SEGMENT13 <> 'T_LYR'
			AND SEGMENT8  <> 'T_LOB'
			AND SEGMENT9  <> 'T_PAR'
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------BEGIN INTEGRITY CHECK-----------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------Tests for records in Oracle that are not in BPC-------------------------------------------
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		SIGNEDDATA,'in Oracle but not BPC' AS STATUS		
FROM(
		SELECT a.Company,a.Account,a.CostCentre,a.Intco,a.Lob,a.Lossyear,a.Par,a.Product,a.Program,a.Project,a.Province,
		a.Reinsurer,a.Treaty,a.TIMEID,a.CATEGORY,a.SIGNEDDATA
		FROM dbo.TMPBPC_STAGE a--INLINE TABLE FUNCTION PERFORMS ETL
		
		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY,SIGNEDDATA
				FROM TBLFACTMAIN
				WHERE LEFT(TIMEID,4) >= (SELECT VALUE FROM tblDefaults WHERE KeyID = 'PERIOD_YEAR_TO_LOAD')
				and CATEGORY in ('ACTUAL','ACTUAL_CON'))
	)W
UNION
----------------------------Tests for records in Oracle that are not in BPC-------------------------------------------
SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,
		SIGNEDDATA,'in BPC but not Oracle' AS STATUS
FROM(
		SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY,SIGNEDDATA
		FROM TBLFACTMAIN
		WHERE LEFT(TIMEID,4) >= (SELECT VALUE FROM tblDefaults WHERE KeyID = 'PERIOD_YEAR_TO_LOAD')
		and CATEGORY in ('ACTUAL','ACTUAL_CON')
		EXCEPT
				(SELECT Company,Account,CostCentre,Intco,Lob,Lossyear,Par,Product,Program,Project,Province,Reinsurer,Treaty,TIMEID,CATEGORY,SIGNEDDATA
				FROM dbo.TMPBPC_STAGE a)
		)x
END

