﻿



CREATE procedure [dbo].[A_CLEAR_POOLTEMP] @whatever nvarchar(10)

as	

/*
A_CLEAR_POOLTEMP Stored Procedure
This procedure will delete all records in the POOLING_TEMP data source.
It is intended to be run after pooling logic has written to this temporary data source.
By deleting these temporary records we will not fill up the writeback table with clutter.

Copyright 2015 Column5 Consulting
Created by Stefan Dunhem
Contact:  sdunhem@column5.com
*/

--Delete records

delete from dbo.tblFACTWBMain
WHERE DATASRC = 'POOLING_TEMP'
delete from dbo.tblFAC2Main
WHERE DATASRC = 'POOLING_TEMP'
delete from dbo.tblFACTMain
WHERE DATASRC = 'POOLING_TEMP'

--End of Stored Procedure




