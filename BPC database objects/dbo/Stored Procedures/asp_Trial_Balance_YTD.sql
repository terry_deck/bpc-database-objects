﻿




CREATE PROCEDURE [dbo].[asp_Trial_Balance_YTD] (@company nvarchar(7), @category nvarchar(20), @rundate nvarchar(6))

AS
SET NOCOUNT ON;

-----  Preparing Tables  -----

BEGIN
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[asp_TrialBalanceReport]') AND type in (N'U'))
DROP TABLE [dbo].[asp_TrialBalanceReport]

CREATE TABLE [dbo].[asp_TrialBalanceReport](
	[ACCOUNTDESC] [nvarchar](50) NOT NULL,
	[COMPANY] [nvarchar](20) NOT NULL,
	[COSTCENTRE] [nvarchar](20) NOT NULL,
	[ACCOUNT] [nvarchar](20) NOT NULL,
	[INTCO] [nvarchar](20) NOT NULL,
	[PROVINCE] [nvarchar](20) NOT NULL,
	[PROGRAM] [nvarchar](20) NOT NULL,
	[PRODUCT] [nvarchar](20) NOT NULL,
	[LOB] [nvarchar](20) NOT NULL,
	[PAR] [nvarchar](20) NOT NULL,
	[PROJECT] [nvarchar](20) NOT NULL,
	[TREATY] [nvarchar](20) NOT NULL,
	[REINSURER] [nvarchar](20) NOT NULL,
	[LOSSYEAR] [nvarchar](20) NOT NULL,
	[CATEGORY] [nvarchar](20) NOT NULL,
	[Begining] [decimal](25, 10) NOT NULL,
	[Period] [decimal](25, 10) NOT NULL,
	[YTD] [decimal](25, 10) NOT NULL,
	[TIMEID] [nvarchar](20) NOT NULL
) ON [PRIMARY]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[asp_TrialBalanceRptAccts]') AND type in (N'U'))
DROP TABLE [dbo].[asp_TrialBalanceRptAccts]

CREATE TABLE [dbo].[asp_TrialBalanceRptAccts](
	[ACCOUNTDESC] [nvarchar](50) NOT NULL,
	[ACCOUNT] [nvarchar](20) NOT NULL,
	[ACCTFLAG] [nvarchar](2) NOT NULL,
	[CATEGORY] [nvarchar](20) NOT NULL,
	[COMPANY] [nvarchar](20) NOT NULL,
	[COSTCENTRE] [nvarchar](20) NOT NULL,
	[LOB] [nvarchar](20) NOT NULL,
	[LOSSYEAR] [nvarchar](20) NOT NULL,
	[PAR] [nvarchar](20) NOT NULL,
	[PRODUCT] [nvarchar](20) NOT NULL,
	[PROGRAM] [nvarchar](20) NOT NULL,
	[PROJECT] [nvarchar](20) NOT NULL,
	[PROVINCE] [nvarchar](20) NOT NULL,
	[REINSURER] [nvarchar](20) NOT NULL,
	[TREATY] [nvarchar](20) NOT NULL,
	[INTCO] [nvarchar](20) NOT NULL
) ON [PRIMARY]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[asp_TrialBalanceRptFact]') AND type in (N'U'))
DROP TABLE [dbo].[asp_TrialBalanceRptFact]

CREATE TABLE [dbo].[asp_TrialBalanceRptFact](
	[ACCOUNTDESC] [nvarchar](50) NOT NULL,
	[ACCOUNT] [nvarchar](20) NOT NULL,
	[ACCTFLAG] [nvarchar](2) NOT NULL,
	[CATEGORY] [nvarchar](20) NOT NULL,
	[TIMEID] [nvarchar](20) NOT NULL,
	[SIGNEDDATA] [decimal](25, 10) NOT NULL,
	[COMPANY] [nvarchar](20) NOT NULL,
	[COSTCENTRE] [nvarchar](20) NOT NULL,
	[LOB] [nvarchar](20) NOT NULL,
	[LOSSYEAR] [nvarchar](20) NOT NULL,
	[PAR] [nvarchar](20) NOT NULL,
	[PRODUCT] [nvarchar](20) NOT NULL,
	[PROGRAM] [nvarchar](20) NOT NULL,
	[PROJECT] [nvarchar](20) NOT NULL,
	[PROVINCE] [nvarchar](20) NOT NULL,
	[REINSURER] [nvarchar](20) NOT NULL,
	[TREATY] [nvarchar](20) NOT NULL,
	[INTCO] [nvarchar](20) NOT NULL
) ON [PRIMARY]

END


BEGIN  -- Populate asp_TrialBalanceRptAccts and asp_TrialBalanceRptFact table

	DECLARE @preYear AS nvarchar(8)
	DECLARE @year AS NUMERIC
	DECLARE @mth NUMERIC

	-- Previous Year (Balance Sheet Accts)--
    SELECT @preYear = CONVERT(nvarchar, CAST(SUBSTRING(@runDate, 1, 4) AS INT)-1) + '1200'

	SET @year = CAST(SUBSTRING(@rundate, 1, 4) AS INT);
	SET @mth = CAST(SUBSTRING(@rundate, 5, 2) AS INT);

	Truncate table asp_TrialBalanceRptAccts
	Truncate table asp_TrialBalanceRptFact

	-- Declare varibles for cursor
	DECLARE @acctName NVARCHAR(50)
	DECLARE @com NVARCHAR(20);
	DECLARE @cct NVARCHAR(20);
	DECLARE @acc NVARCHAR(20);
	DECLARE @intco NVARCHAR(20);
	DECLARE @prov NVARCHAR(20);
	DECLARE @prog NVARCHAR(20);
	DECLARE @prod NVARCHAR(20);
	DECLARE @lob NVARCHAR(20);
	DECLARE @par NVARCHAR(20);
	DECLARE @proj NVARCHAR(20);
	DECLARE @trt NVARCHAR(20);
	DECLARE @rei NVARCHAR(20);
	DECLARE @lyr NVARCHAR(20);
	DECLARE @period MONEY;
	DECLARE @beginning MONEY;
	DECLARE @ytd MONEY;
	DECLARE @acctFlag NVARCHAR(2);

	---------   Balance Sheet Accounts  ----------------  
	-- ALL AVAILABLE IS ACCTOUS BETWEEN JAN and RUN MONTH --
	SET @acctFlag = 'BS';

	INSERT INTO asp_TrialBalanceRptAccts
		   ([ACCOUNTDESC]
		   ,[ACCOUNT]
		   ,[ACCTFLAG] -- 'BS' for Balance Sheet Accounts
		   ,[CATEGORY]
		   ,[COMPANY]
		   ,[COSTCENTRE]
		   ,[LOB]
		   ,[LOSSYEAR]
		   ,[PAR]
		   ,[PRODUCT]
		   ,[PROGRAM]
		   ,[PROVINCE]
		   ,[PROJECT]
		   ,[REINSURER]
		   ,[TREATY]
		   ,[INTCO])

	SELECT distinct  
	b.EVDESCRIPTION AS AccountName
	,a.Account AS Account
	, @acctFlag AS ACCTFLAG
	, @category AS CATEGORY
	, a.COMPANY AS Company
	, a.CostCentre AS CostCentre
	, a.LOB AS LOB
	, a.LOSSYEAR AS LossYear
	, a.Par AS Par
	, a.PRODUCT AS Product
	, a.Program AS Program
	, a.Province AS Province
	, a.Project AS Project
	, a.REINSURER AS Reinsurer
	, a.Treaty AS Treaty
	, a.Intco AS Intco

	FROM tblFactMain a

	INNER JOIN mbrAccount b
	  ON a.ACCOUNT = b.ID
	  AND b.ACCTYPE in ('AST', 'LEQ')
	  AND b.ID not like '%_MAIN'

	WHERE a.CATEGORY = @category 
	AND a.COMPANY=@company 
	AND a.TIMEID in (@preYear, (@rundate+'00'))
	AND a.ACCOUNT not like '%_MAIN'

	ORDER BY a.[COMPANY]
			   ,a.[COSTCENTRE]
			   ,a.[ACCOUNT]
			   ,a.[INTCO]
			   ,a.[PROVINCE]
			   ,a.[PROGRAM]
			   ,a.[PRODUCT]
			   ,a.[LOB]
			   ,a.[PAR]
			   ,a.[PROJECT]
			   ,a.[TREATY]
			   ,a.[REINSURER]
			   ,a.[LOSSYEAR]
	               
			

	---------------------------INCOME STATEMENT ACCOUNTS-------------------------------------
	-- ALL AVAILABLE IS ACCTOUS BETWEEN JAN and RUN MONTH--
	SET @acctFlag = 'IS';

	INSERT INTO asp_TrialBalanceRptAccts
		   ([ACCOUNTDESC]
		   ,[ACCOUNT]
		   ,[ACCTFLAG] -- 'IS' for Income Statment Accounts'
		   ,[CATEGORY]
		   ,[COMPANY]
		   ,[COSTCENTRE]
		   ,[LOB]
		   ,[LOSSYEAR]
		   ,[PAR]
		   ,[PRODUCT]
		   ,[PROGRAM]
		   ,[PROVINCE]
		   ,[PROJECT]
		   ,[REINSURER]
		   ,[TREATY]
		   ,[INTCO])

	SELECT distinct  
	b.EVDESCRIPTION AS AccountName
	,a.Account AS Account
	, @acctFlag AS ACCTFLAG
	, @category AS CATEGORY
	, a.COMPANY AS Company
	, a.CostCentre AS CostCentre
	, a.LOB AS LOB
	, a.LOSSYEAR AS LossYear
	, a.Par AS Par
	, a.PRODUCT AS Product
	, a.Program AS Program
	, a.Province AS Province
	, a.Project AS Project
	, a.REINSURER AS Reinsurer
	, a.Treaty AS Treaty
	, a.Intco AS Intco


	FROM tblFactMain a

	INNER JOIN mbrAccount b
	  ON a.ACCOUNT = b.ID
	  AND b.ACCTYPE in ('EXP', 'INC')
	  AND b.ID not like '%_MAIN'
	  
	WHERE a.CATEGORY = @category 
	AND a.COMPANY=@company 
	AND a.ACCOUNT not like '%_MAIN'
	AND a.TIMEID between (CONVERT(nvarchar, CAST(SUBSTRING(@runDate, 1, 4) AS INT)-1)+'0100') 
		and (@rundate+'00')
		

	ORDER BY a.[COMPANY]
			   ,a.[COSTCENTRE]
			   ,a.[ACCOUNT]
			   ,a.[INTCO]
			   ,a.[PROVINCE]
			   ,a.[PROGRAM]
			   ,a.[PRODUCT]
			   ,a.[LOB]
			   ,a.[PAR]
			   ,a.[PROJECT]
			   ,a.[TREATY]
			   ,a.[REINSURER]
			   ,a.[LOSSYEAR]



	--------------  GET THE RECORDS FROM FACT TABLE BASED ON ACCTS  --------------------
    DECLARE accounts_cur CURSOR FOR

	SELECT  [ACCOUNTDESC]
			,[COMPANY]
			,[COSTCENTRE]
			,[ACCOUNT]
			,[INTCO]
			,[PROVINCE]
			,[PROGRAM]
			,[PRODUCT]
			,[LOB]
			,[PAR]
			,[PROJECT]
			,[TREATY]
			,[REINSURER]
			,[LOSSYEAR]
			,[ACCTFLAG] 
	        
	FROM asp_TrialBalanceRptAccts
	 
	OPEN accounts_cur;

	FETCH NEXT FROM accounts_cur
	INTO @acctName, @com, @cct, @acc, @intco, @prov, @prog, @prod, @lob, @par, @proj, @trt, @rei, @lyr, @acctFlag;

	WHILE @@FETCH_STATUS = 0
	BEGIN

        IF @acctFlag = 'BS' -- need to retreive balance from previous year end
		BEGIN
			-- Get balance from previous year end
			SET @period = (SELECT distinct 
					CAST(SUM(SIGNEDDATA) AS MONEY) AS PERIOD  -- the total balance of the previous year end
				  FROM tblFactMain
				  WHERE COMPANY = @com
				  AND COSTCENTRE = @cct
				  AND CATEGORY = @category
				  AND ACCOUNT = @acc
				  AND INTCO = @intco
				  AND PROVINCE = @prov
				  AND PROGRAM = @prog
				  AND PRODUCT = @prod
				  AND LOB = @lob
				  AND PAR = @par
				  AND PROJECT = @proj
				  AND TREATY = @trt
				  AND REINSURER = @rei
				  AND LOSSYEAR = @lyr
				  AND TIMEID = @preYear
				  
				  GROUP BY COMPANY
					, COSTCENTRE
					, CATEGORY
					, ACCOUNT
					, INTCO
					, PROVINCE
					, PROGRAM
					, PRODUCT
					, LOB
					, PAR
					, PROJECT
					, TREATY
					, REINSURER
					, LOSSYEAR
					, TIMEID)
			IF @period is null 
				SET @period = 0
	  
			INSERT INTO asp_TrialBalanceRptFact
				   ([ACCOUNTDESC]
				   ,[ACCOUNT]
				   ,[ACCTFLAG] -- 'BS' for Balance Sheet Accounts; 'IS' for Income Statment Accounts'
				   ,[CATEGORY]
				   ,[TIMEID]
				   ,[SIGNEDDATA]
				   ,[COMPANY]
				   ,[COSTCENTRE]
				   ,[LOB]
				   ,[LOSSYEAR]
				   ,[PAR]
				   ,[PRODUCT]
				   ,[PROGRAM]
				   ,[PROVINCE]
				   ,[PROJECT]
				   ,[REINSURER]
				   ,[TREATY]
				   ,[INTCO])
			VALUES (@acctName
					, @acc
					, @acctFlag
					, @category
					, @preYear
					, @period
					, @com
					, @cct
					, @lob
					, @lyr
					, @par
					, @prod
					, @prog
					, @prov
					, @proj
					, @rei
					, @trt
					, @intco)
					
			-- Get balance from current month
			SET @period = (SELECT distinct 
					CAST(SUM(SIGNEDDATA) AS MONEY) AS PERIOD  -- the total balance of the month
				  FROM tblFactMain
				  WHERE COMPANY = @com
				  AND COSTCENTRE = @cct
				  AND CATEGORY = @category
				  AND ACCOUNT = @acc
				  AND INTCO = @intco
				  AND PROVINCE = @prov
				  AND PROGRAM = @prog
				  AND PRODUCT = @prod
				  AND LOB = @lob
				  AND PAR = @par
				  AND PROJECT = @proj
				  AND TREATY = @trt
				  AND REINSURER = @rei
				  AND LOSSYEAR = @lyr
				  AND TIMEID = @rundate+'00'
				  
				  GROUP BY COMPANY
					, COSTCENTRE
					, CATEGORY
					, ACCOUNT
					, INTCO
					, PROVINCE
					, PROGRAM
					, PRODUCT
					, LOB
					, PAR
					, PROJECT
					, TREATY
					, REINSURER
					, LOSSYEAR
					, TIMEID)
			IF @period is null 
				SET @period = 0
	  
			INSERT INTO asp_TrialBalanceRptFact
				   ([ACCOUNTDESC]
				   ,[ACCOUNT]
				   ,[ACCTFLAG] -- 'BS' for Balance Sheet Accounts; 'IS' for Income Statment Accounts'
				   ,[CATEGORY]
				   ,[TIMEID]
				   ,[SIGNEDDATA]
				   ,[COMPANY]
				   ,[COSTCENTRE]
				   ,[LOB]
				   ,[LOSSYEAR]
				   ,[PAR]
				   ,[PRODUCT]
				   ,[PROGRAM]
				   ,[PROVINCE]
				   ,[PROJECT]
				   ,[REINSURER]
				   ,[TREATY]
				   ,[INTCO])
			VALUES (@acctName
					, @acc
					, @acctFlag
					, @category
					, @rundate + '00'
					, @period
					, @com
					, @cct
					, @lob
					, @lyr
					, @par
					, @prod
					, @prog
					, @prov
					, @proj
					, @rei
					, @trt
					, @intco)
		END
		
		IF @acctFlag = 'IS'  -- get previous months from current year
		BEGIN
			DECLARE @mthFlag INT
			DECLARE @mthStr NVARCHAR(8)

			SET @mthFlag = 1
			
			WHILE (@mthFlag <= @mth)
			BEGIN
				IF @mthFlag >9
					SET @mthStr = SUBSTRING(@runDate, 1, 4) + CONVERT(nvarchar, @mthFlag) + '00'
				ELSE
					SET @mthStr = SUBSTRING(@runDate, 1, 4) + '0' + CONVERT(nvarchar, @mthFlag)	+ '00'

				SET @period = (SELECT distinct 
						CAST(SUM(SIGNEDDATA) AS MONEY) AS PERIOD  -- the total balance of the month
					  FROM tblFactMain
					  WHERE COMPANY = @com
					  AND COSTCENTRE = @cct
					  AND CATEGORY = @category
					  AND ACCOUNT = @acc
					  AND INTCO = @intco
					  AND PROVINCE = @prov
					  AND PROGRAM = @prog
					  AND PRODUCT = @prod
					  AND LOB = @lob
					  AND PAR = @par
					  AND PROJECT = @proj
					  AND TREATY = @trt
					  AND REINSURER = @rei
					  AND LOSSYEAR = @lyr
					  AND TIMEID = @mthStr
					  
					  GROUP BY COMPANY
						, COSTCENTRE
						, CATEGORY
						, ACCOUNT
						, INTCO
						, PROVINCE
						, PROGRAM
						, PRODUCT
						, LOB
						, PAR
						, PROJECT
						, TREATY
						, REINSURER
						, LOSSYEAR
						, TIMEID)
				IF @period is null 
					SET @period = 0
				
				INSERT INTO asp_TrialBalanceRptFact
					   ([ACCOUNTDESC]
					   ,[ACCOUNT]
					   ,[ACCTFLAG] -- 'BS' for Balance Sheet Accounts; 'IS' for Income Statment Accounts'
					   ,[CATEGORY]
					   ,[TIMEID]
					   ,[SIGNEDDATA]
					   ,[COMPANY]
					   ,[COSTCENTRE]
					   ,[LOB]
					   ,[LOSSYEAR]
					   ,[PAR]
					   ,[PRODUCT]
					   ,[PROGRAM]
					   ,[PROVINCE]
					   ,[PROJECT]
					   ,[REINSURER]
					   ,[TREATY]
					   ,[INTCO])
				VALUES (@acctName
						, @acc
						, @acctFlag
						, @category
						, @mthStr
						, @period
						, @com
						, @cct
						, @lob
						, @lyr
						, @par
						, @prod
						, @prog
						, @prov
						, @proj
						, @rei
						, @trt
						, @intco)
					
				SET @mthFlag = @mthFlag + 1
			END
		END
		
		FETCH NEXT FROM accounts_cur
		INTO @acctName, @com, @cct, @acc, @intco, @prov, @prog, @prod, @lob, @par, @proj, @trt, @rei, @lyr, @acctFlag;

	END;

	CLOSE accounts_cur;
	DEALLOCATE accounts_cur;

-- End of Populate asp_TrialBalanceRptAccts and asp_TrialBalanceRptFact table

-- Generate Trial Balance Report for Balance Sheet accounts
	Truncate table asp_TrialBalanceReport
	DECLARE balance_sheet_cur CURSOR FOR
	 
	SELECT distinct 
	  a.ACCOUNTDESC AS AccountName
	, a.COMPANY AS Company
	, a.CostCentre AS CostCentre
	, a.Account AS Account
	, a.Intco AS Intco
	, a.Province AS Province
	, a.Program AS Program
	, a.PRODUCT AS Product
	, a.LOB AS LOB
	, a.Par AS Par
	, a.Project AS Project
	, a.Treaty AS Treaty
	, a.REINSURER AS Reinsurer
	, a.LOSSYEAR AS LossYear
	, CAST(a.SIGNEDDATA AS MONEY) AS YTD

	FROM asp_TrialBalanceRptFact a

	WHERE a.CATEGORY = @category 
	AND a.ACCTFLAG = 'BS'
	AND a.COMPANY=@company 
	AND a.TIMEID=(@rundate+'00')

	ORDER BY a.[COMPANY]
			   ,a.[COSTCENTRE]
			   ,a.[ACCOUNT]
			   ,a.[INTCO]
			   ,a.[PROVINCE]
			   ,a.[PROGRAM]
			   ,a.[PRODUCT]
			   ,a.[LOB]
			   ,a.[PAR]
			   ,a.[PROJECT]
			   ,a.[TREATY]
			   ,a.[REINSURER]
			   ,a.[LOSSYEAR]

	OPEN balance_sheet_cur;

	FETCH NEXT FROM balance_sheet_cur
	INTO @acctName, @com, @cct, @acc, @intco, @prov, @prog, @prod, @lob, @par, @proj, @trt, @rei, @lyr, @ytd;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @beginning = (
			SELECT CAST(a.SIGNEDDATA AS MONEY) FROM asp_TrialBalanceRptFact a
				WHERE a.ACCOUNT=@acc
				  AND a.CATEGORY = @category
				  AND a.COMPANY = @com
				  AND a.CostCentre = @cct
				  AND a.Intco = @intco
				  AND a.Province = @prov
				  AND a.Program = @prog
				  AND a.PRODUCT = @prod
				  AND a.LOB = @lob
				  AND a.Par = @par
				  AND a.Project = @proj
				  AND a.Treaty = @trt
				  AND a.REINSURER = @rei
				  AND a.LOSSYEAR = @lyr
				  AND a.TIMEID = @preYear
				  AND a.ACCTFLAG = 'BS'
			  );

		If @beginning Is null
		  SET @beginning = 0;
			  
		If @ytd Is Null
		  SET @ytd = 0;
			
		SET @period = @ytd - @beginning

		INSERT INTO asp_TrialBalanceReport 
			   ([ACCOUNTDESC]
			   ,[COMPANY]
			   ,[COSTCENTRE]
			   ,[ACCOUNT]
			   ,[INTCO]
			   ,[PROVINCE]
			   ,[PROGRAM]
			   ,[PRODUCT]
			   ,[LOB]
			   ,[PAR]
			   ,[PROJECT]
			   ,[TREATY]
			   ,[REINSURER]
			   ,[LOSSYEAR]
			   ,[CATEGORY]
			   ,[Begining]
			   ,[Period]
			   ,[YTD]
			   ,[TIMEID])
		 VALUES
			   (@acctName
			   , SUBSTRING(@com, 1, 3)
			   , SUBSTRING(@cct, 1, 4)
			   , SUBSTRING(@acc, 1, 6)
			   , SUBSTRING(@intco, 1, 3)
			   , SUBSTRING(@prov, 1, 2)
			   , SUBSTRING(@prog, 1, 4)
			   , SUBSTRING(@prod, 1, 2)
			   , SUBSTRING(@lob, 1, 3)
			   , SUBSTRING(@par, 1, 1)
			   , SUBSTRING(@proj, 1, 4)
			   , SUBSTRING(@trt, 1, 4)
			   , SUBSTRING(@rei, 1, 3)
			   , SUBSTRING(@lyr, 1, 4)
			   , @category
			   , @beginning
			   , @period
			   , @ytd
			   , @rundate+'00');
	               
		
		FETCH NEXT FROM balance_sheet_cur
		INTO @acctName, @com, @cct, @acc, @intco, @prov, @prog, @prod, @lob, @par, @proj, @trt, @rei, @lyr, @ytd;

	END;
	CLOSE balance_sheet_cur;
	DEALLOCATE balance_sheet_cur;
-- End of Generate Trial Balance Report for Balance Sheet accounts

	-------------------------INCOME STATEMENT ACCOUNTS-------------------------------------

	DECLARE income_stmt_cur CURSOR FOR
	 
	SELECT  
	a.ACCOUNTDESC AS AccountName
	, a.COMPANY AS Company
	, a.CostCentre AS CostCentre
	, a.Account AS Account
	, a.Intco AS Intco
	, a.Province AS Province
	, a.Program AS Program
	, a.PRODUCT AS Product
	, a.LOB AS LOB
	, a.Par AS Par
	, a.Project AS Project
	, a.Treaty AS Treaty
	, a.REINSURER AS Reinsurer
	, a.LOSSYEAR AS LossYear
	, CAST(a.SIGNEDDATA AS MONEY) AS PERIOD

	FROM asp_TrialBalanceRptFact a

	WHERE a.CATEGORY = @category 
	AND a.ACCTFLAG = 'IS'
	AND a.COMPANY=@company 
	AND a.TIMEID=(@rundate+'00')

	ORDER BY a.[COMPANY]
			   ,a.[COSTCENTRE]
			   ,a.[ACCOUNT]
			   ,a.[INTCO]
			   ,a.[PROVINCE]
			   ,a.[PROGRAM]
			   ,a.[PRODUCT]
			   ,a.[LOB]
			   ,a.[PAR]
			   ,a.[PROJECT]
			   ,a.[TREATY]
			   ,a.[REINSURER]
			   ,a.[LOSSYEAR]

	OPEN income_stmt_cur;

	FETCH NEXT FROM income_stmt_cur
	INTO @acctName, @com, @cct, @acc, @intco, @prov, @prog, @prod, @lob, @par, @proj, @trt, @rei, @lyr, @period;

	WHILE @@FETCH_STATUS = 0
	BEGIN
       SET @beginning = 0;

	   SET @period = (
			SELECT SUM(CAST(b.SIGNEDDATA AS MONEY)) FROM asp_TrialBalanceRptFact b
				WHERE b.ACCOUNT=@acc
				  AND b.CATEGORY = @category
				  AND b.COMPANY = @com
				  AND b.CostCentre = @cct
				  AND b.Intco = @intco
				  AND b.Province = @prov
				  AND b.Program = @prog
				  AND b.PRODUCT = @prod
				  AND b.LOB = @lob
				  AND b.Par = @par
				  AND b.Project = @proj
				  AND b.Treaty = @trt
				  AND b.REINSURER = @rei
				  AND b.LOSSYEAR = @lyr
				  AND b.TIMEID between @year+'0100' and @rundate+'00'

				GROUP BY b.ACCOUNT
				  , b.CATEGORY
				  , b.COMPANY
				  , b.CostCentre
				  , b.Intco
				  , b.Province
				  , b.Program
				  , b.PRODUCT
				  , b.LOB
				  , b.Par
				  , b.Project
				  , b.Treaty
				  , b.REINSURER
				  , b.LOSSYEAR

				  );
			  
		IF @period Is Null
		  SET @period = 0;
		
		SET @ytd = @beginning + @period;

		INSERT INTO asp_TrialBalanceReport 
			   ([ACCOUNTDESC]
			   ,[COMPANY]
			   ,[COSTCENTRE]
			   ,[ACCOUNT]
			   ,[INTCO]
			   ,[PROVINCE]
			   ,[PROGRAM]
			   ,[PRODUCT]
			   ,[LOB]
			   ,[PAR]
			   ,[PROJECT]
			   ,[TREATY]
			   ,[REINSURER]
			   ,[LOSSYEAR]
			   ,[CATEGORY]
			   ,[Begining]
			   ,[Period]
			   ,[YTD]
			   ,[TIMEID])
		 VALUES
			   (@acctName
			   , SUBSTRING(@com, 1, 3)
			   , SUBSTRING(@cct, 1, 4)
			   , SUBSTRING(@acc, 1, 6)
			   , SUBSTRING(@intco, 1, 3)
			   , SUBSTRING(@prov, 1, 2)
			   , SUBSTRING(@prog, 1, 4)
			   , SUBSTRING(@prod, 1, 2)
			   , SUBSTRING(@lob, 1, 3)
			   , SUBSTRING(@par, 1, 1)
			   , SUBSTRING(@proj, 1, 4)
			   , SUBSTRING(@trt, 1, 4)
			   , SUBSTRING(@rei, 1, 3)
			   , SUBSTRING(@lyr, 1, 4)
			   , @category
			   , @beginning
			   , @period
			   , @ytd
			   , @rundate+'00');
	           
		FETCH NEXT FROM income_stmt_cur
		INTO @acctName, @com, @cct, @acc, @intco, @prov, @prog, @prod, @lob, @par, @proj, @trt, @rei, @lyr, @period;

	END;
	CLOSE income_stmt_cur;
	DEALLOCATE income_stmt_cur;

END   