﻿
CREATE PROC [dbo].[csp_MetaAuto_GetUpd] (
	  @src_tbl sysname  
	, @trg_tbl sysname 
	, @control_id nvarchar(50)
	, @dim_nm nvarchar(50)
	, @mods_tbl sysname 
) as

-- delcare local vars
declare 
	  @cur_dim nvarchar(50)
	, @trg_fld sysname
	, @hier_lvl smallint
	, @upd_where nvarchar(500)
	, @tmp_tbl sysname
	, @sql nvarchar(2000)

-- declare/open cursor for the fields
declare upd_flds cursor fast_forward for
	select dim_nm, trg_fld, hier_lvl, case isnull(upd_where,N'') when N'' then N'1=1' else upd_where end  
	from mbrzzzMetaMapping 
	where UPPER(updatable) IN ('1','Y','YES')
	  and control_id = @control_id
	order by hier_lvl;
open upd_flds;

-- get the first row
fetch next from upd_flds
	into @cur_dim, @trg_fld, @hier_lvl, @upd_where;


-- compare field for the certain level and write the chg record if it differs
while @@fetch_status = 0 begin

	-- first join (mbrzzzMetaMapping to metaTmp{DIM}): restrict the set to records specified for automation
	-- second join (metaTmp{DIM} to mbr{DIM}): find matching ID's and check for field change and upd_where condition
	set @sql = N'-- ' + @trg_fld + N' test
	insert ' + @mods_tbl + N' (mod_type, dim_nm, fld_nm, hier_lvl, id, src_val, dst_val)	
		select N''UPD'', N''' + @cur_dim + N''', N''' + @trg_fld + N''', source.autometa_hier_lvl, target.ID, source.' + @trg_fld + N', target.' + @trg_fld + N'
		from mbrzzzMetaMapping detail inner join ' + @src_tbl + N' source
		  on detail.control_id = N''' + @control_id + N'''
		 and detail.trg_fld = N''' + @trg_fld + N'''
		 and detail.hier_lvl = ' + convert(varchar,@hier_lvl) + N'
		 and detail.hier_lvl = source.autometa_hier_lvl	
		inner join ' + @trg_tbl + N' target
		  on target.ID = source.ID
		where source.' + @trg_fld + N'<>target.' + @trg_fld + N' COLLATE SQL_Latin1_General_Cp1_CS_AS
		  and ' + @upd_where

	print @sql
	exec( @sql )

	if @@error <> 0 begin
		close upd_flds
		deallocate upd_flds
		return
	end

	-- next rec
	fetch next from upd_flds
		into @cur_dim, @trg_fld, @hier_lvl, @upd_where;
end

close upd_flds
deallocate upd_flds

/*

DECLARE	@return_value int

EXEC	@return_value = [dbo].[csp_MetaAuto_GetChg]
		@src_tbl = metaTmpCustomer,
		@trg_tbl = mbrCustomer,
		@dim_nm = N'Customer',
		@mods_tbl = 'autometa_mods_5'

SELECT	'Return Value' = @return_value
exec ('select * from ' + @mods_tbl)

*/

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_GetTblNames]    Script Date: 08/13/2008 01:17:33 ******/
SET ANSI_NULLS ON

