﻿CREATE PROC [dbo].[csp_MetaAuto_MakeModsTbl] (
	  @src_tbl sysname 
	, @trg_tbl sysname
	, @unique_id int = 1
	, @mods_tbl sysname OUTPUT
) as

-- delcare local vars
declare 
	  @sql nvarchar(1000)
	, @max_len nvarchar(5)

-- init
select @mods_tbl = 'metaMods_' + convert(varchar,@unique_id)

-- get the max len for changed values
select @max_len = convert(nvarchar,max(ml)) from 
	-- get the maximum property size
	(select max(character_maximum_length) ml from information_schema.columns
	where table_name = @trg_tbl and character_maximum_length is not null
		union 
	select max(character_maximum_length) ml from information_schema.columns
	where table_name = @src_tbl and character_maximum_length is not null) a

-- drop & recreate a changed fields table
if object_id(@mods_tbl) is not null
	exec(N'drop table ' + @mods_tbl)

set @sql = N'create table ' + @mods_tbl + N' (
	  mod_type varchar(3) not null
	, dim_nm nvarchar(50) not null
	, fld_nm sysname null
	, hier_lvl smallint null
	, id nvarchar(20) not null
	, src_val nvarchar(' + @max_len + N') null
	, dst_val nvarchar(' + @max_len + N') null
)'

exec(@sql)

/* Sample call:

DECLARE	@return_value int,
		@mods_tbl sysname

EXEC	@return_value = [dbo].[csp_MetaAuto_MakeModsTbl]
		@src_tbl = metaTmpCustomer,
		@trg_tbl = mbrCustomer,
		@unique_id = 5,
		@mods_tbl = @mods_tbl OUTPUT

SELECT	@mods_tbl as N'@mods_tbl'

SELECT	'Return Value' = @return_value
*/

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_GetUpdAll]    Script Date: 08/13/2008 01:17:40 ******/
SET ANSI_NULLS ON
