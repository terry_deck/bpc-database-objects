﻿CREATE   proc [dbo].[csp_Aud_ProcLogAdd] (
	  @main_aud_log_id int
	, @proc_name sysname
	, @action_item varchar(255)
	, @status char(1) = 'I'
) AS

set nocount on

insert aud_proc_log ( aud_main_log_id, proc_name, action_item, status )
	select @main_aud_log_id, @proc_name, @action_item, @status

return @@IDENTITY

/* Sample call:

DECLARE	@return_value int

EXEC	@return_value = [dbo].[csp_Aud_ProcLogAdd]
		@main_aud_log_id = 1,
		@proc_name = csp_MetaAuto_GetChg,
		@action_item = N'find changes'

SELECT	'Return Value' = @return_value

*/

/****** Object:  StoredProcedure [dbo].[csp_Aud_ProcLogUpd]    Script Date: 04/10/2008 23:42:04 ******/
SET ANSI_NULLS ON
