﻿



CREATE PROCEDURE [dbo].[asp_DETAILED_USER_LIST] 
AS
SET NOCOUNT ON;
-----  Preparing Temp Table  -----
BEGIN
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[asp_USER_REPORT_T1]') AND type in (N'U'))
DROP TABLE [dbo].[asp_USER_REPORT_T1]

CREATE TABLE [dbo].[asp_USER_REPORT_T1](
[UserID]  [nvarchar](100) NOT NULL,
[FullName] [nvarchar](200) NOT NULL, 
[Teams]	[nvarchar](200),
[TaskProfile] [nvarchar](100),
[MAP] [nvarchar](100)
) ON [PRIMARY]

END;


BEGIN  -- cycle through user table

	-- Declare variables for cursors
	DECLARE @uID NVARCHAR(100);
	DECLARE @fname NVARCHAR(200);
	DECLARE @MAPs NVARCHAR(100);
	DECLARE @MAPs2 NVARCHAR(100);
	DECLARE @Team NVARCHAR(200);
	DECLARE @Team2 NVARCHAR(200);
	DECLARE @Team3 NVARCHAR(200);
	DECLARE @Tasks NVARCHAR(100);
	DECLARE @Tasks2 NVARCHAR(100);
	
	DECLARE user_cur CURSOR FOR

	SELECT  [UserID],
	        [FullName]
	FROM  [dbo].[tblUsers]
		 
	OPEN user_cur;

	FETCH NEXT FROM user_cur
	INTO @uID, @fname;

	WHILE @@FETCH_STATUS = 0

	BEGIN		
		   -- cycle through profile table looking for user MAPs
		   
	       DECLARE map_cur CURSOR FOR

		   SELECT  [ProfileID]
		   FROM  [dbo].[UserProfile]
		   where UserID = @uID
		   and Inheritance = 'N'
		   and ProfileClass = 'MBR'
		 
		   OPEN map_cur;

		   FETCH NEXT FROM map_cur
		   INTO @MAPs;

		   WHILE @@FETCH_STATUS = 0
		   BEGIN
				INSERT into [dbo].[asp_USER_REPORT_T1]
				VALUES (@uID, @fname,'','',@MAPS);

				FETCH NEXT FROM map_cur
				INTO @MAPs;
	       END;
	
	       CLOSE map_cur;
	       DEALLOCATE map_cur;

		   -- cycle through profile table looking for user TASKSs
	       DECLARE task_cur CURSOR FOR

		   SELECT  [ProfileID]
		   FROM  [dbo].[UserProfile]
		   where UserID = @uID
		   and Inheritance = 'N'
		   and ProfileClass = 'TSK'
		 
		   OPEN task_cur;

		   FETCH NEXT FROM task_cur
		   INTO @tasks;

		   WHILE @@FETCH_STATUS = 0
		   BEGIN
				INSERT into [dbo].[asp_USER_REPORT_T1]
				VALUES (@uID, @fname,'',@tasks,'');

				FETCH NEXT FROM task_cur
				INTO @tasks;
	       END;
	
	       CLOSE task_cur;
	       DEALLOCATE task_cur;

		   		   -- cycle through profile table looking for team MAPs
	       DECLARE map2_cur CURSOR FOR

		   SELECT  [ProfileID]
		   FROM  [dbo].[UserProfile]
		   where UserID = @uID
		   and Inheritance = 'Y'
		   and ProfileClass = 'MBR'
		 
		   OPEN map2_cur;

		   FETCH NEXT FROM map2_cur
		   INTO @MAPs2;

		   WHILE @@FETCH_STATUS = 0
		   BEGIN
				DECLARE team_cur CURSOR FOR

				SELECT UserOrTeamID
				from [UserTeamProfileAssign] a
				where [ProfileID] = @MAPs2
				and [IsATeam] = 'Y'
				and @uID in (Select userorteamid from [dbo].[UserTeamAssign] where MemberOfTeamID = a.UserOrTeamID)

				OPEN team_cur;

				FETCH NEXT FROM team_cur
		        INTO @Team;

				WHILE @@FETCH_STATUS = 0
				BEGIN

					INSERT into [dbo].[asp_USER_REPORT_T1]
					VALUES (@uID, @fname,@Team,'',@MAPs2);

					FETCH NEXT FROM team_cur
					INTO @Team;
	            END;
			    CLOSE team_cur;
	            DEALLOCATE team_cur;
				FETCH NEXT FROM map2_cur
				INTO @MAPs2;
		   END;
	       CLOSE map2_cur;
	       DEALLOCATE map2_cur;

		   -- cycle through profile table looking for team TASKSs
	       DECLARE tasks2_cur CURSOR FOR

		   SELECT  [ProfileID]
		   FROM  [dbo].[UserProfile]
		   where UserID = @uID
		   and Inheritance = 'Y'
		   and ProfileClass = 'TSK'
		 
		   OPEN tasks2_cur;

		   FETCH NEXT FROM tasks2_cur
		   INTO @Tasks2;

		   WHILE @@FETCH_STATUS = 0
		   BEGIN
				DECLARE team2_cur CURSOR FOR
				
				SELECT UserOrTeamID
				from [dbo].[UserTeamProfileAssign] a
				where [ProfileID] = @Tasks2
				and [IsATeam] = 'Y'
				and @uID in (Select userorteamid from [dbo].[UserTeamAssign] where MemberOfTeamID = a.UserOrTeamID)

				OPEN team2_cur;

				FETCH NEXT FROM team2_cur
		                INTO @Team2;

				WHILE @@FETCH_STATUS = 0
				BEGIN

					INSERT into [dbo].[asp_USER_REPORT_T1]
					VALUES (@uID, @fname,@Team2,@Tasks2,'');

					FETCH NEXT FROM team2_cur
					INTO @Team2; 
				END;
				
				CLOSE team2_cur;
				DEALLOCATE team2_cur;
				
		    FETCH NEXT FROM tasks2_cur
		    INTO @Tasks2;

		    END;
				       	  
		    CLOSE tasks2_cur;
		    DEALLOCATE tasks2_cur;

			-- cycle through team table looking for teams with no profiles
			DECLARE team3_cur CURSOR FOR
				
				SELECT MemberOfTeamID
				from [dbo].[UserTeamAssign] a
				where a.[UserOrTeamID] = @uID
				and a.[MemberofTeamID] not in (Select UserOrTeamID from [dbo].[UserTeamProfileAssign])

				OPEN team3_cur;

				FETCH NEXT FROM team3_cur
		                INTO @Team3;

				WHILE @@FETCH_STATUS = 0
				BEGIN

					INSERT into [dbo].[asp_USER_REPORT_T1]
					VALUES (@uID, @fname,@Team3,'','');

					FETCH NEXT FROM team3_cur
					INTO @Team3; 
				END;
				
			CLOSE team3_cur;
			DEALLOCATE team3_cur;

	FETCH NEXT FROM user_cur
	INTO @uID, @fname;

	END;

	CLOSE user_cur;
	DEALLOCATE user_cur;

-- End of Cycle through user table


END

