﻿


------------------------------BPC MERGE STAGING Tables------------------------------------------------------
---THIS PROCEDURE IS USED TO merge records from dbo.aBPC_STAGE_INC into 
--- dbo.aBPC_STAGE  The merge function loads the data if none currently exists for
--- that record's BPC dimensionality.  If a record already exists it replaces the signeddata
--- amount from the amount from the record in the aBPC_STAGE_INC table.
--- dimensionality.
--- 22/08/2012 T Deck
--- Created stored procedure
--------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[asp_BPC_MERGE_STAGE]
    
AS

-- Added this update step to set the amount from oracle back to it's original
-- value.  In the asp_BPC_ETL_INC this same step is executed to reverse the
-- sign for creation of the flat file. This returns the value to normal prior
-- to merge with a_BPC_STAGE. 
UPDATE [dbo].[aBPC_STAGE_INC]
SET signeddata = signeddata*-1
where currency = 'STAT'
and account in ('EPPR00_ACC','FTPR00_ACC','GWPR00_ACC')

MERGE dbo.aBPC_STAGE AS target
USING (SELECT LEDGER_ID,COMPANY,COSTCENTRE,ACCOUNT,INTCO,PROVINCE,PROGRAM,
              PRODUCT,LOB,PAR,PROJECT,TREATY,REINSURER,LOSSYEAR,A_TYPE, 
              CURRENCY,A_FLAG,PERIOD_NAME,PERIOD_YEAR,PERIOD_NUM,SOURCE,
              DATASRC,CATEGORY,TIME,TIMEID, SIGNEDDATA 
       FROM dbo.aBPC_STAGE_INC) AS source(LEDGER_ID,COMPANY,COSTCENTRE,
										  ACCOUNT,INTCO,PROVINCE,PROGRAM,
										  PRODUCT,LOB,PAR,PROJECT,TREATY,
										  REINSURER,LOSSYEAR,A_TYPE,CURRENCY,
										  A_FLAG,PERIOD_NAME,PERIOD_YEAR,
										  PERIOD_NUM,SOURCE,DATASRC,CATEGORY,
										  TIME,TIMEID,SIGNEDDATA) 
ON (target.LEDGER_ID = source.LEDGER_ID
    and target.COMPANY = source.COMPANY
    and target.COSTCENTRE = source.COSTCENTRE
    and target.ACCOUNT = source.ACCOUNT
    and target.INTCO = source.INTCO
    and target.PROVINCE = source.PROVINCE
    and target.PROGRAM = source.PROGRAM
    and target.PRODUCT = source.PRODUCT
    and target.LOB = source.LOB
    and target.PAR = source.PAR
    and target.PROJECT = source.PROJECT
    and target.TREATY = source.TREATY
    and target.REINSURER= source.REINSURER
    and target.LOSSYEAR = source.LOSSYEAR
    and target.A_TYPE = source.A_TYPE
    and target.CURRENCY = source.CURRENCY
    and target.A_FLAG = source.A_FLAG
    and target.PERIOD_NAME = source.PERIOD_NAME
    and target.PERIOD_YEAR = source.PERIOD_YEAR
    and target.PERIOD_NUM = source.PERIOD_NUM
    and target.SOURCE = source.SOURCE
    and target.DATASRC = source.DATASRC
    and target.CATEGORY = source.CATEGORY
    and target.TIME = source.TIME
    and target.TIMEID = source.TIMEID)
WHEN MATCHED 
    THEN UPDATE SET target.SIGNEDDATA = source.SIGNEDDATA 
WHEN NOT MATCHED THEN
	INSERT (LEDGER_ID,COMPANY,COSTCENTRE,ACCOUNT,INTCO,PROVINCE,PROGRAM,PRODUCT,
	        LOB,PAR,PROJECT,TREATY,REINSURER,LOSSYEAR,A_TYPE,CURRENCY,A_FLAG,
	        PERIOD_NAME,PERIOD_YEAR,PERIOD_NUM,SOURCE,DATASRC,CATEGORY,TIME,
	        TIMEID,SIGNEDDATA)
	VALUES (source.LEDGER_ID,source.COMPANY,source.COSTCENTRE,source.ACCOUNT,
	        source.INTCO,source.PROVINCE,source.PROGRAM,source.PRODUCT,source.LOB,
	        source.PAR,source.PROJECT,source.TREATY,source.REINSURER,
	        source.LOSSYEAR,source.A_TYPE,source.CURRENCY,source.A_FLAG,
	        source.PERIOD_NAME,source.PERIOD_YEAR,source.PERIOD_NUM,source.SOURCE,
	        source.DATASRC,source.CATEGORY,source.TIME,source.TIMEID,
	        source.SIGNEDDATA)
;


