﻿




------------------------------BPC EXTRACTION------------------------------------------------------
---THIS PROCEDURE IS USED TO EXTACT BPC DATA FROM THE TBLMAINFACT TABLES AND MASSAGE THE DATA INTO
---A FORMAT COMPATIBLE FOR ORACLE AND WRITE THE DATA TO A STAGING TABLE
---THE PROCEDURE DOES THE FOLLOWING:
---1) CREATES THE STAGING TABLE IF IT DOESNT EXIST
---2) TRANSFORMS BPC COLUMNS TO MATCH ORACLE ACCOUNT SEGMENTS
---3) TRUNCATES AND INSERTS TRANSFORMED RECORDS INTO A BPC_STAGE TABLE
--- CHANGE LOG
--- 05/10/2011 T Deck
--- Created new for bpc 7.5 and new COA changes.
--- 24/11/2011 T Deck   Added new stat accounts and new datasrc.
--- 29/11/2011 T Deck   Changed table definition for DB and CR fields to (25,10)
---                     Changed all data assignments to DB and CR fields to use 10
---                     decimal places (same as BPC fact tables)
--- 12/12/2011 T Deck   Added new stat accounts.  Changed mapping for 9 strategic
---                     Stat accounts from mapping to one Oracle acct to 1:1 mapping
--- 21/11/2012 M Lorenc Removed some STAT accounts. Added / excluded datasources.
---						Added Companies.
--------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[asp_CLIC_BPC_EXTRACTION](@company nvarchar(100), @buddate nvarchar(6))

AS
SET NOCOUNT ON;

IF NOT EXISTS(SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'aORACLE_STAGE')

BEGIN
CREATE TABLE dbo.aORACLE_STAGE(--CREATES TABLE IF IT DOESN'T EXIST
[SOB]         NVARCHAR(3) DEFAULT N'190',
[COMP]        NVARCHAR(6),
[BUD]         NVARCHAR(6) DEFAULT N'Budget',
[ITEMDATE]    NVARCHAR(11),
[ACTFLAG]     NVARCHAR(1) DEFAULT N'B',
[CUR]         NVARCHAR(4) DEFAULT N'CAD',
[COMPANY]     NVARCHAR(3),
[COSTCENTRE]  NVARCHAR(4),
[ACCOUNT]     NVARCHAR(6),
[INTCO]       NVARCHAR(3),
[PROVINCE]    NVARCHAR(2),
[PROGRAM]     NVARCHAR(4),
[PRODUCT]     NVARCHAR(2),
[LOB]         NVARCHAR(3),
[PAR]         NVARCHAR(1),
[PROJECT]     NVARCHAR(4),
[TREATY]      NVARCHAR(4),
[REINSURER]   NVARCHAR(3),
[LOSSYEAR]    NVARCHAR(4),
[DB]          DECIMAL(25,10),
[CR]          DECIMAL(25,10),
[PER]         NVARCHAR(6),
[F1]          NVARCHAR(1) NULL,
[BATCH]       NVARCHAR(25),
[F2]		  NVARCHAR(1) NULL,
JOURNAL		  NVARCHAR(25),
[F3]          NVARCHAR(1) NULL,
[F4]          NVARCHAR(1) NULL,
[F5]          NVARCHAR(1) NULL,
[BUDNAME]     NVARCHAR(15),
[F6]          NVARCHAR(1) NULL,
[DATASRC]     NVARCHAR(20),
[CATEGORY]    NVARCHAR(20),
[OMONTH]       NVARCHAR(3),
[OYEAR]        NVARCHAR(4),
[FOLDER]	  NVARCHAR(4),
[SOURCE]      NVARCHAR(25))

END
ELSE
BEGIN
TRUNCATE TABLE DBO.aORACLE_STAGE
END

BEGIN

INSERT INTO DBO.aORACLE_STAGE ([CUR],[COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],
							[PROGRAM],[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],
							[REINSURER],[LOSSYEAR],[ITEMDATE],[OMONTH],[OYEAR],
							[FOLDER],[COMP],[SOURCE],[DB],[CR],[PER],[BATCH],[JOURNAL],[BUDNAME])

-- Sets Currency to Stat for selected accounts else CAD
SELECT (CASE WHEN [ACCOUNT] = 'NUAP00_ACC' THEN 'STAT'
			 -- added 02/08/2011
			 WHEN [ACCOUNT] = 'DPCT00_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'FINSYS_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'HEAD00_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'HRZN00_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'PRMDRT_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'PSTV00_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'STAF00_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'XPN000_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'XPNF00_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'XPNS00_ACC' THEN 'STAT'
			 -- end of additions
			 -- added 07/09/2014
			 WHEN [ACCOUNT] = 'WGHT00_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'PRMC00_ACC' THEN 'STAT'
			 WHEN [ACCOUNT] = 'FTE_FULL' THEN 'STAT'
			 WHEN [ACCOUNT] = 'FTE_PART' THEN 'STAT'
			 WHEN [ACCOUNT] = 'FTE_TEMP' THEN 'STAT'
			 WHEN [ACCOUNT] = 'FTE_TEMP_NOBEN' THEN 'STAT'
			 WHEN [ACCOUNT] = 'HED_FULL' THEN 'STAT'
			 WHEN [ACCOUNT] = 'HED_PAID_LEAVE' THEN 'STAT'
			 WHEN [ACCOUNT] = 'HED_PART' THEN 'STAT'
			 WHEN [ACCOUNT] = 'HED_TEMP' THEN 'STAT'
			 WHEN [ACCOUNT] = 'HED_UNPAID_LEAVE' THEN 'STAT'
			 -- end of additions
			 ELSE 'CAD'
			 END) AS CUR
	  , SUBSTRING([COMPANY],1,3) 
	  -- Sets costcentre to 0000 if BPC cost center is BPCD else use the existing costcentre
	  ,(CASE WHEN SUBSTRING([COSTCENTRE],1,4)= 'BPCD' THEN '0000'
	    ELSE SUBSTRING([COSTCENTRE],1,4)
	    END) AS COSTCENTRE
	  -- Sets the Oracle account value for selected BPC stat accounts else use existing account
	  ,(CASE WHEN [ACCOUNT] = 'FTE_FULL' THEN 'BFTE00'
			 WHEN [ACCOUNT] = 'FTE_PART' THEN 'BFTE00'
			 WHEN [ACCOUNT] = 'FTE_TEMP' THEN 'BFTE00'
			 WHEN [ACCOUNT] = 'FTE_NON_DRIVER' THEN 'BFTE00'
			 WHEN [ACCOUNT] = 'FTE_TEMP_NOBEN' THEN 'BFTE00'
			 WHEN [ACCOUNT] = 'FTE_IMPACT_ACC' THEN 'BFTE00'
			 WHEN [ACCOUNT] = 'HED_FULL' THEN 'HEAD00'
			 WHEN [ACCOUNT] = 'HED_PART' THEN 'HEAD00'
			 WHEN [ACCOUNT] = 'HED_TEMP' THEN 'HEAD00'
			 WHEN [ACCOUNT] = 'HED_PAID_LEAVE' THEN 'HEAD00'
			 WHEN [ACCOUNT] = 'HED_UNPAID_LEAVE' THEN 'HEAD00'
			 ELSE SUBSTRING([ACCOUNT],1,6)
			 END) AS ACCOUNT  
	  ,SUBSTRING([INTCO],1,3)  
	  ,SUBSTRING([PROVINCE],1,2)  
	  ,SUBSTRING([PROGRAM],1,4)  
	  ,SUBSTRING([PRODUCT],1,2)  
	  ,SUBSTRING([LOB],1,3) 
	  ,SUBSTRING([PAR],1,1)  
	  ,SUBSTRING([PROJECT],1,4)  
	  ,SUBSTRING([TREATY],1,4) 
	  ,SUBSTRING([REINSURER],1,3)  
	  ,SUBSTRING([LOSSYEAR],1,4)  
	  --creates transaction date from BPC timeid always 1st of the month
      ,'01-'+ (CASE WHEN SUBSTRING([TIMEID],5,2) = '01' THEN 'JAN'
				   WHEN SUBSTRING([TIMEID],5,2) = '02' THEN 'FEB'
				   WHEN SUBSTRING([TIMEID],5,2) = '03' THEN 'MAR'
				   WHEN SUBSTRING([TIMEID],5,2) = '04' THEN 'APR'
				   WHEN SUBSTRING([TIMEID],5,2) = '05' THEN 'MAY'
				   WHEN SUBSTRING([TIMEID],5,2) = '06' THEN 'JUN'
				   WHEN SUBSTRING([TIMEID],5,2) = '07' THEN 'JUL'
				   WHEN SUBSTRING([TIMEID],5,2) = '08' THEN 'AUG'
				   WHEN SUBSTRING([TIMEID],5,2) = '09' THEN 'SEP'
				   WHEN SUBSTRING([TIMEID],5,2) = '10' THEN 'OCT'
				   WHEN SUBSTRING([TIMEID],5,2) = '11' THEN 'NOV'
				   WHEN SUBSTRING([TIMEID],5,2) = '12' THEN 'DEC'
			       ELSE 'JAN' 
			       END) + '-'+ SUBSTRING([TIMEID],1,4) as ITEMDATE
	  -- converts numeric month to 3 character abbreviation
	  ,(CASE WHEN SUBSTRING([TIMEID],5,2) = '01' THEN 'JAN'
				   WHEN SUBSTRING([TIMEID],5,2) = '02' THEN 'FEB'
				   WHEN SUBSTRING([TIMEID],5,2) = '03' THEN 'MAR'
				   WHEN SUBSTRING([TIMEID],5,2) = '04' THEN 'APR'
				   WHEN SUBSTRING([TIMEID],5,2) = '05' THEN 'MAY'
				   WHEN SUBSTRING([TIMEID],5,2) = '06' THEN 'JUN'
				   WHEN SUBSTRING([TIMEID],5,2) = '07' THEN 'JUL'
				   WHEN SUBSTRING([TIMEID],5,2) = '08' THEN 'AUG'
				   WHEN SUBSTRING([TIMEID],5,2) = '09' THEN 'SEP'
				   WHEN SUBSTRING([TIMEID],5,2) = '10' THEN 'OCT'
				   WHEN SUBSTRING([TIMEID],5,2) = '11' THEN 'NOV'
				   WHEN SUBSTRING([TIMEID],5,2) = '12' THEN 'DEC'
			       ELSE 'JAN' 
			       END) as OMONTH
      ,	SUBSTRING([TIMEID],1,4) as OYEAR
      -- sets folder variable which is the folder on ftp site where this should go
      , ( Case WHEN [COMPANY] = '001_COM' THEN 'CGIC'
		       WHEN [COMPANY] = '003_COM' THEN 'CGIC'
		       WHEN [COMPANY] = '021_COM' THEN 'CGIC'
		       WHEN [COMPANY] = '029_COM' THEN 'SOV'
		       WHEN [COMPANY] = '032_COM' THEN 'UC'
		       WHEN [COMPANY] = '040_COM' THEN 'UC'
		       WHEN [COMPANY] = '045_COM' THEN 'HB'
		       WHEN [COMPANY] = '049_COM' THEN 'HB'
		       WHEN [COMPANY] = '050_COM' THEN 'HB'
		       WHEN [COMPANY] = '055_COM' THEN 'HB'
		       WHEN [COMPANY] = '056_COM' THEN 'HB'
		       WHEN [COMPANY] = '057_COM' THEN 'HB'
		       WHEN [COMPANY] = '060_COM' THEN 'CLIC'
		       WHEN [COMPANY] = '065_COM' THEN 'CLIC'
		       WHEN [COMPANY] = '067_COM' THEN 'CLIC'
		       WHEN [COMPANY] = '068_COM' THEN 'CLIC'
		       WHEN [COMPANY] = '078_COM' THEN 'CLIC'
		       WHEN [COMPANY] = '090_COM' THEN 'CUMS'
		       WHEN [COMPANY] = '091_COM' THEN 'CUMS'
		       WHEN [COMPANY] = '092_COM' THEN 'CUMS'
		       WHEN [COMPANY] = '096_COM' THEN 'CUMS'
		       WHEN [COMPANY] = '0EA_COM' THEN 'CGIC'
		       END) as FOLDER
		-- uses company dimension value to set company abbreviation used in Oracle
	  , SUBSTRING([COMPANY],1,3) as COMP
		-- uses company dimension value to set source used in Oracle
	  , ( Case WHEN [COMPANY] = '001_COM' THEN 'CGL'
	  		   WHEN [COMPANY] = '003_COM' THEN 'CFSL'
		       WHEN [COMPANY] = '021_COM' THEN 'CGIC'
		       WHEN [COMPANY] = '029_COM' THEN 'SOV'
		       WHEN [COMPANY] = '032_COM' THEN 'UC'
		       WHEN [COMPANY] = '040_COM' THEN 'EQ'
		       WHEN [COMPANY] = '045_COM' THEN 'COS'
		       WHEN [COMPANY] = '049_COM' THEN '9208-9648 Quebec Inc'
		       WHEN [COMPANY] = '050_COM' THEN '9175-1917 Quebec Inc'
		       WHEN [COMPANY] = '055_COM' THEN 'HB'
		       WHEN [COMPANY] = '056_COM' THEN 'FED'
		       WHEN [COMPANY] = '057_COM' THEN 'UNIFED'
		       WHEN [COMPANY] = '060_COM' THEN 'CLIC'
		       WHEN [COMPANY] = '065_COM' THEN 'TIC'
		       WHEN [COMPANY] = '067_COM' THEN 'SelectCare Worldwide Corp'
		       WHEN [COMPANY] = '068_COM' THEN 'TIC Eliminations'
		       WHEN [COMPANY] = '078_COM' THEN 'CLIC Travel Eliminations'
		       WHEN [COMPANY] = '090_COM' THEN 'CUMIS Life'
		       WHEN [COMPANY] = '091_COM' THEN 'CUMIS General'
		       WHEN [COMPANY] = '092_COM' THEN 'CUMIS Group'
		       WHEN [COMPANY] = '096_COM' THEN 'CUMIS Eliminations'
		       WHEN [COMPANY] = '0EA_COM' THEN 'B P Company'
		       END) as SRC
	   --  creates db field from BPC signeddata reverses sign for select accounts
	  --,(CASE WHEN(CASE WHEN [ACCOUNT] = '405000_ACC' THEN [SIGNEDDATA]*-1
	  --				   WHEN [ACCOUNT] = '405005_ACC' THEN [SIGNEDDATA]*-1
	  --				   ELSE [SIGNEDDATA]
	  --				   END) > 0 THEN  [SIGNEDDATA]
	  --		 ELSE 0.0000000000 
	  --       END) as DB
	  -- creates db field from BPC signeddata.
	  ,(CASE WHEN [SIGNEDDATA] > 0 THEN [SIGNEDDATA]
	         ELSE 0.0000000000
	         END) as DB
	   -- creates cr field from BPC signeddata reverses sign for select accounts
	  --,(CASE WHEN(CASE WHEN [ACCOUNT] = '405000_ACC' THEN [SIGNEDDATA]*-1
	  --				   WHEN [ACCOUNT] = '405005_ACC' THEN [SIGNEDDATA]*-1
	  --				   ELSE [SIGNEDDATA]
	  --				   END) < 0 THEN  ABS([SIGNEDDATA])
	  --		 ELSE 0.0000000000 
	  --       END) as CR
	  -- creates cr field from BPC signeddata
	  ,(CASE WHEN [SIGNEDDATA] < 0 THEN ABS([SIGNEDDATA])
	         ELSE 0.0000000000
	         END) as CR
	   -- creates Oracle period from month and year of BPC timeid
	  ,(CASE WHEN SUBSTRING([TIMEID],5,2) = '01' THEN 'JAN'
				   WHEN SUBSTRING([TIMEID],5,2) = '02' THEN 'FEB'
				   WHEN SUBSTRING([TIMEID],5,2) = '03' THEN 'MAR'
				   WHEN SUBSTRING([TIMEID],5,2) = '04' THEN 'APR'
				   WHEN SUBSTRING([TIMEID],5,2) = '05' THEN 'MAY'
				   WHEN SUBSTRING([TIMEID],5,2) = '06' THEN 'JUN'
				   WHEN SUBSTRING([TIMEID],5,2) = '07' THEN 'JUL'
				   WHEN SUBSTRING([TIMEID],5,2) = '08' THEN 'AUG'
				   WHEN SUBSTRING([TIMEID],5,2) = '09' THEN 'SEP'
				   WHEN SUBSTRING([TIMEID],5,2) = '10' THEN 'OCT'
				   WHEN SUBSTRING([TIMEID],5,2) = '11' THEN 'NOV'
				   WHEN SUBSTRING([TIMEID],5,2) = '12' THEN 'DEC'
			       ELSE 'JAN' 
			       END)+'-'+SUBSTRING([TIMEID],3,2)as PER
       -- creates a batch id from month, year and company 	
	  ,(CASE WHEN SUBSTRING([TIMEID],5,2) = '01' THEN 'JAN'
				   WHEN SUBSTRING([TIMEID],5,2) = '02' THEN 'FEB'
				   WHEN SUBSTRING([TIMEID],5,2) = '03' THEN 'MAR'
				   WHEN SUBSTRING([TIMEID],5,2) = '04' THEN 'APR'
				   WHEN SUBSTRING([TIMEID],5,2) = '05' THEN 'MAY'
				   WHEN SUBSTRING([TIMEID],5,2) = '06' THEN 'JUN'
				   WHEN SUBSTRING([TIMEID],5,2) = '07' THEN 'JUL'
				   WHEN SUBSTRING([TIMEID],5,2) = '08' THEN 'AUG'
				   WHEN SUBSTRING([TIMEID],5,2) = '09' THEN 'SEP'
				   WHEN SUBSTRING([TIMEID],5,2) = '10' THEN 'OCT'
				   WHEN SUBSTRING([TIMEID],5,2) = '11' THEN 'NOV'
				   WHEN SUBSTRING([TIMEID],5,2) = '12' THEN 'DEC'
			       ELSE 'JAN' 
			       END)+' '+SUBSTRING([TIMEID],1,4)+' '+( Case WHEN [COMPANY] = '001_COM' THEN 'CGL'
		       WHEN [COMPANY] = '001_COM' THEN 'CGL'
	  		   WHEN [COMPANY] = '003_COM' THEN 'CFSL'
		       WHEN [COMPANY] = '021_COM' THEN 'CGIC'
		       WHEN [COMPANY] = '029_COM' THEN 'SOV'
		       WHEN [COMPANY] = '032_COM' THEN 'UC'
		       WHEN [COMPANY] = '040_COM' THEN 'EQ'
		       WHEN [COMPANY] = '045_COM' THEN 'COS'
		       WHEN [COMPANY] = '049_COM' THEN '9208'
		       WHEN [COMPANY] = '050_COM' THEN '9175'
		       WHEN [COMPANY] = '055_COM' THEN 'HB'
		       WHEN [COMPANY] = '056_COM' THEN 'FED'
		       WHEN [COMPANY] = '057_COM' THEN 'UNIFED'
		       WHEN [COMPANY] = '060_COM' THEN 'CLIC'
		       WHEN [COMPANY] = '065_COM' THEN 'TIC'
		       WHEN [COMPANY] = '067_COM' THEN 'SELC'
		       WHEN [COMPANY] = '068_COM' THEN 'TIC Elim'
		       WHEN [COMPANY] = '078_COM' THEN 'CLIC Elim'
		       WHEN [COMPANY] = '090_COM' THEN 'CUMISLife'
		       WHEN [COMPANY] = '091_COM' THEN 'CUMISGen'
		       WHEN [COMPANY] = '092_COM' THEN 'CUMISGrp'
		       WHEN [COMPANY] = '096_COM' THEN 'CUMISElim'
		       WHEN [COMPANY] = '0EA_COM' THEN 'B P Comp'
		       END)+' Budget' as BATCH
	   -- creates a journal id from month, year and company 
	  ,(CASE WHEN SUBSTRING([TIMEID],5,2) = '01' THEN 'JAN'
				   WHEN SUBSTRING([TIMEID],5,2) = '02' THEN 'FEB'
				   WHEN SUBSTRING([TIMEID],5,2) = '03' THEN 'MAR'
				   WHEN SUBSTRING([TIMEID],5,2) = '04' THEN 'APR'
				   WHEN SUBSTRING([TIMEID],5,2) = '05' THEN 'MAY'
				   WHEN SUBSTRING([TIMEID],5,2) = '06' THEN 'JUN'
				   WHEN SUBSTRING([TIMEID],5,2) = '07' THEN 'JUL'
				   WHEN SUBSTRING([TIMEID],5,2) = '08' THEN 'AUG'
				   WHEN SUBSTRING([TIMEID],5,2) = '09' THEN 'SEP'
				   WHEN SUBSTRING([TIMEID],5,2) = '10' THEN 'OCT'
				   WHEN SUBSTRING([TIMEID],5,2) = '11' THEN 'NOV'
				   WHEN SUBSTRING([TIMEID],5,2) = '12' THEN 'DEC'
			       ELSE 'JAN' 
			       END)+' '+SUBSTRING([TIMEID],1,4)+' '+( Case WHEN [COMPANY] = '001_COM' THEN 'CGL'
		       WHEN [COMPANY] = '001_COM' THEN 'CGL'
	  		   WHEN [COMPANY] = '003_COM' THEN 'CFSL'
		       WHEN [COMPANY] = '021_COM' THEN 'CGIC'
		       WHEN [COMPANY] = '029_COM' THEN 'SOV'
		       WHEN [COMPANY] = '032_COM' THEN 'UC'
		       WHEN [COMPANY] = '040_COM' THEN 'EQ'
		       WHEN [COMPANY] = '045_COM' THEN 'COS'
		       WHEN [COMPANY] = '049_COM' THEN '9208'
		       WHEN [COMPANY] = '050_COM' THEN '9175'
		       WHEN [COMPANY] = '055_COM' THEN 'HB'
		       WHEN [COMPANY] = '056_COM' THEN 'FED'
		       WHEN [COMPANY] = '057_COM' THEN 'UNIFED'
		       WHEN [COMPANY] = '060_COM' THEN 'CLIC'
		       WHEN [COMPANY] = '065_COM' THEN 'TIC'
		       WHEN [COMPANY] = '067_COM' THEN 'SELC'
		       WHEN [COMPANY] = '068_COM' THEN 'TIC Elim'
		       WHEN [COMPANY] = '078_COM' THEN 'CLIC Elim'
		       WHEN [COMPANY] = '090_COM' THEN 'CUMISLife'
		       WHEN [COMPANY] = '091_COM' THEN 'CUMIS Gen'
		       WHEN [COMPANY] = '092_COM' THEN 'CUMIS Grp'
		       WHEN [COMPANY] = '096_COM' THEN 'CUMISElim'
		       WHEN [COMPANY] = '0EA_COM' THEN 'B P Comp'
		       END)+' Budget' as JOURNAL
	  -- creates the budget name as CGC year Budget
	  ,'CGC '+SUBSTRING([TIMEID],1,4)+' Budget' as BUDNAME
   FROM [tblFactMain]
   WHERE [CATEGORY] = 'BUDGET'
   AND [DATASRC] in ('BPC_INPUT','MAIN_JE','CLIC_IMPORT','TIC_IMPORT','SC_IMPORT',
                     'ORACLE','SP_INPUT','ALLOC_DEBIT','ALLOC_CREDIT','RP_PROVIDER',
                     'RP_RECEIVER','CONSOL_JE','CUMISLFE_IMPORT','CUMISGEN_IMPORT',
                     'CUMISGRP_IMPORT','CUMISELM_IMPORT')
   AND [ACCOUNT] NOT LIKE '1%'
   AND [ACCOUNT] NOT LIKE '2%'
   AND [ACCOUNT] NOT LIKE '4%'
   AND TIMEID like @buddate+'%'
   AND COMPANY in (@company)
    

   END   






