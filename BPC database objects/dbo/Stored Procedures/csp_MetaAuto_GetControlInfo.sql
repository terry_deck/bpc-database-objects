﻿CREATE proc [dbo].[csp_MetaAuto_GetControlInfo] (
	  @control_id nvarchar(20)
	, @src_tbl sysname OUTPUT
	, @tmp_tbl sysname OUTPUT
	, @trg_tbl sysname OUTPUT
	, @dim_nm nvarchar(125) OUTPUT
	, @del_or_flag nvarchar(1) OUTPUT
	, @del_prop sysname OUTPUT
	, @del_prop_val nvarchar(255) OUTPUT
) as

select 
	  @src_tbl = src_tbl_nm
	, @tmp_tbl = tmp_tbl_nm
	, @trg_tbl = trg_tbl_nm
	, @dim_nm = dim_nm
	, @del_or_flag = del_or_flag
	, @del_prop = del_prop
	, @del_prop_val = del_prop_val
from mbrzzzMetaControl
where ID = @control_id

select * from mbrzzzMetaControl

/* Sample call:

DECLARE	@return_value int,
		@src_tbl sysname,
		@tmp_tbl sysname,
		@trg_tbl sysname,
		@dim_nm nvarchar(125)

EXEC	@return_value = [dbo].[csp_MetaAuto_GetTblNames]
		@control_id = N'CustomerMain',
		@src_tbl = @src_tbl OUTPUT,
		@tmp_tbl = @tmp_tbl OUTPUT,
		@trg_tbl = @trg_tbl OUTPUT,
		@dim_nm = @dim_nm OUTPUT

SELECT	@src_tbl as N'@src_tbl',
		@tmp_tbl as N'@tmp_tbl',
		@trg_tbl as N'@trg_tbl',
		@dim_nm as N'@dim_nm'

*/

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_GetDel]    Script Date: 08/13/2008 01:16:37 ******/
SET ANSI_NULLS ON
