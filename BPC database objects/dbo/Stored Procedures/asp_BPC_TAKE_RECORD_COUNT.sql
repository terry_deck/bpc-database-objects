﻿



------------------------------asp_BPC_TAKE_RECORD_COUNT-------------------------------------------
---THIS PROCEDURE IS USED to write record counts from all applications
---to table application_rec_counts.  This is used to track application growth in the database
---
--- 31/08/2011 T Deck
--------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[asp_BPC_TAKE_RECORD_COUNT] 
AS


BEGIN
INSERT INTO [PROD_COBPC].[dbo].[application_rec_counts]
           ([count_date]
           ,[application]
           ,[rec_count])
     VALUES
           (GETDATE(), 'Main', (select COUNT(*) from tblFactMain));
INSERT INTO [PROD_COBPC].[dbo].[application_rec_counts]
           ([count_date]
           ,[application]
           ,[rec_count])
     VALUES
           (GETDATE(), 'Consol', (select COUNT(*) from tblFactConsol));
INSERT INTO [PROD_COBPC].[dbo].[application_rec_counts]
           ([count_date]
           ,[application]
           ,[rec_count])
     VALUES
           (GETDATE(), 'PremClaim', (select COUNT(*) from tblFactPremClaim));
INSERT INTO [PROD_COBPC].[dbo].[application_rec_counts]
           ([count_date]
           ,[application]
           ,[rec_count])
     VALUES
           (GETDATE(), 'SalaryPlanning', (select COUNT(*) from tblFactSalaryPlanning));
INSERT INTO [PROD_COBPC].[dbo].[application_rec_counts]
           ([count_date]
           ,[application]
           ,[rec_count])
     VALUES
           (GETDATE(), 'ShareService', (select COUNT(*) from tblFactSharedService)) ; 
END


