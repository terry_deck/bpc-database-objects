﻿CREATE PROC [dbo].[csp_MetaAuto_ProcessNew] (
	  @mods_tbl sysname 
	, @tmp_tbl sysname 
	, @trg_tbl sysname 
	, @dim_nm nvarchar(125) 
	, @main_log_id int = 18
) AS 

set nocount on

declare 
	 @sql nvarchar(max)
	, @fld_list nvarchar(max)
	, @fld sysname
	, @select_fld_list nvarchar(max)

-- validate parms

-- get field names common to both tables
select @fld_list = dbo.cfnGetFields(@trg_tbl, 1, N'[SEQ]') -- must incl brackets on excluded fields
SET @select_fld_list = REPLACE (REPLACE (@fld_list,'[','ISNULL(['),']','],'''')')

-- insert new records
-- Get the complete list of fields from the tmp table but get the list of new records from the @mods_tbl
set @sql = 'insert ' + @trg_tbl + ' (' + @fld_list + ')
select ' + @select_fld_list + ' from ' + @tmp_tbl + ' tmp
inner join (select ID autometa_new_id from ' + @mods_tbl + ' where dim_nm = ''' + @dim_nm + ''' and mod_type = ''ADD'') mods
	on tmp.ID = mods.autometa_new_id'

print @sql
--select @fld_list, @trg_tbl, @tmp_tbl, @mods_tbl, @dim_nm, @sql
exec (@sql)

return @@rowcount
/*
declare @return_value int
EXEC	@return_value = [dbo].[csp_MetaAuto_ProcessNew]
			@mods_tbl = 'metaMods_1',
			@tmp_tbl = 'metaTmpHRPosition',
			@trg_tbl = 'mbrHRPosition',
			@dim_nm = 'HRPosition',
			@main_log_id = 1
*/

/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_ProcessDel]    Script Date: 08/13/2008 01:17:49 ******/
SET ANSI_NULLS ON
