﻿

------------------------------BPC STAGE ETL INC------------------------------------------------------
---THIS PROCEDURE IS USED TO FEED THE BPC LOAD PROCESS FOR DATA LOADED IN ORACLE SINCE THE LAST LOAD TO BPC.
---THE START YEAR IS USER DEFINED FROM THE BPC APPLICATION PARAMATER PAGE.  IT LOADS
---DATA FOR THE YEARS ENTERED.  THE TABLE CONTAINS ALL RELEVANT BPC DIMENSIONAL COLUMNS
---AND SOME EXTRANEOUS FIELDS FOUND IN THE ORACLE GL TABLE.  THESE ARE USED PRIMARILY IN THE
---TRANSFORMATION PROCESS.  THE PROCEDURE DOES THE FOLLOWING:
---1) CREATES THE STAGING TABLE IF IT DOESNT EXIST
---2) CALLS THE ORACLE GL SOURCE TABLES DIRECTLY IN ORACLE USING OPENQUERY
---3) TRANSFORMS ORACLE COLUMNS TO MATCH BPC DIMENSIONALITY
---4) DETERMINES SIGNEDDATA VALUE FROM CR/DR BALANCES AND ACCT TYPE
---5) TRUNCATES AND INSERTS TRANSFORMED RECORDS INTO ABPC_STAGE_INC TABLE
---6) ABPC_STAGE_INC MEETS BPC LOAD REQUIREMENTS
---7) Inserts a record into aBPC_LOAD_STATS to track stats of load
---8) Changes sign on certain stat accounts in ABPC_STAGE_INC
--- 15/04/2014 T Deck
--- Removed Consol ledger for new consol implementation 2013-0054
--- 30/10/2014 T Deck
--- Changed code so that we would not get all the zero balances for future months when
--- new periods are opened in Oracle
--- 1/10/2014 T Deck
--- changed code so that it now correctly picks up current period and previous period 
--- balances which were booked by the same person who opened the period in Oracle
--------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[asp_BPC_ETL_INC] @lastrun nvarchar(24)

AS
SET NOCOUNT ON;
DECLARE @lr varchar(max)
DECLARE @TSQL varchar(max)
DECLARE @TSQL1a varchar(max)
DECLARE @TSQL2 varchar(max)
DECLARE @TSQL3  varchar(max)
DECLARE @TSQL4  varchar(max)
DECLARE @TSQL5 nvarchar(max)
DECLARE @TSQL6 nvarchar(max)
DECLARE @TSQL7 nvarchar(max)
DECLARE @TSQL8 nvarchar(max)
DECLARE @PYEAR varchar(max)
DECLARE @curmth varchar(max)
DECLARE @premth varchar(max)
SELECT @curmth = (SELECT  Upper(substring(convert(varchar(25), getdate(), 6),4,3))+'-'+substring(convert(varchar(25), getdate(), 6),8,2))
SELECT @premth = (SELECT  Upper(substring(convert(varchar(25), DATEADD(MONTH, DATEDIFF(MONTH, 0, CURRENT_TIMESTAMP) - 1, 0), 6),4,3))+'-'+substring(convert(varchar(25), DATEADD(MONTH, DATEDIFF(MONTH, 0, CURRENT_TIMESTAMP) - 1, 0), 6),8,2))
SELECT @lr = SUBSTRING(@lastrun,1,19)
SELECT @lr = SUBSTRING(@lastrun,1,19)
--SELECT @lr = SUBSTRING('2014-09-22 09:58:00.000',1,19)
SELECT @PYEAR = CAST((SELECT Value FROM tblDefaults WHERE KeyID = 'PERIOD_YEAR_TO_LOAD')as int) 
SELECT @TSQL = 'INSERT INTO DBO.aBPC_STAGE_INC ([LEDGER_ID],[COMPANY],[COSTCENTRE],[ACCOUNT],[INTCO],[PROVINCE],[PROGRAM],[PRODUCT],[LOB],[PAR],[PROJECT],[TREATY],[REINSURER],[LOSSYEAR],[A_TYPE],[CURRENCY],[A_FLAG],[PERIOD_NAME],[PERIOD_YEAR],[PERIOD_NUM],[SOURCE],[DATASRC],[CATEGORY],[TIME],[TIMEID],[SIGNEDDATA],[LUPDATE])
SELECT CAST([LEDGER_ID]AS NVARCHAR(20)),
CAST(SEGMENT1+''_COM'' AS NVARCHAR(20)) AS COMPANY,
CAST(SEGMENT2+''_CCT'' AS NVARCHAR(20)) AS COSTCENTRE,
CAST(SEGMENT3+''_ACC'' AS NVARCHAR(20)) AS ACCOUNT,
CAST(SEGMENT4+''_ICO'' AS NVARCHAR(20)) AS INTCO,
CAST(SEGMENT5+''_PRV'' AS NVARCHAR(20))AS PROVINCE,
CAST(SEGMENT6+''_PRG'' AS NVARCHAR(20))AS PROGRAM,
CAST(SEGMENT7+''_PRD'' AS NVARCHAR(20))AS PRODUCT,
CAST(SEGMENT8+''_LOB'' AS NVARCHAR(20))AS LOB,
CAST(SEGMENT9+''_PAR'' AS NVARCHAR(20)) AS PAR,
CAST(SEGMENT10+''_PRJ'' AS NVARCHAR(20))AS PROJECT,
CAST(SEGMENT11+''_TRT'' AS NVARCHAR(20)) AS TREATY,
CAST(SEGMENT12+''_REI'' AS NVARCHAR(20)) AS REINSURER,
CAST(SEGMENT13+''_LYR'' AS NVARCHAR(20)) AS LOSSYEAR,
CAST(ACCOUNT_TYPE AS NVARCHAR(20)) AS A_TYPE,
CAST(CURRENCY_CODE AS NVARCHAR(20)) AS CURRENCY,
CAST(ACTUAL_FLAG AS NVARCHAR(20))AS A_FLAG,
PERIOD_NAME,
CAST(PERIOD_YEAR AS NVARCHAR(20)),
CAST(PERIOD_NUM AS NVARCHAR(20)),
''0'' AS SOURCE,
CAST(''ORACLE'' AS NVARCHAR(20)) AS DATASRC,
CAST(''ACTUAL'' AS NVARCHAR(20)) AS CATEGORY,
CASE--ADDED IN CASE THIS TABLE IS LOADED USING BPC IMPORT SQL PACKAGE
WHEN PERIOD_NUM =1 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.JAN'' 
WHEN PERIOD_NUM =2 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.FEB'' 
WHEN PERIOD_NUM =3 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.MAR'' 
WHEN PERIOD_NUM =4 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.APR'' 
WHEN PERIOD_NUM =5 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.MAY'' 
WHEN PERIOD_NUM =6 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.JUN'' 
WHEN PERIOD_NUM =7 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.JUL'' 
WHEN PERIOD_NUM =8 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.AUG'' 
WHEN PERIOD_NUM =9 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.SEP'' 
WHEN PERIOD_NUM =10 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.OCT''
WHEN PERIOD_NUM =11 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.NOV''
WHEN PERIOD_NUM =12 THEN CAST(PERIOD_YEAR AS NVARCHAR(20))+''.DEC''
ELSE ''''
END [TIME],
CAST(PERIOD_YEAR AS NVARCHAR(20))+ 
CASE 
WHEN LEN(CAST(PERIOD_NUM AS NVARCHAR(20))) = 1 THEN ''0''+CAST(period_num AS NVARCHAR(20)) ELSE CAST(period_num AS NVARCHAR(20)) END +
''00'' AS TIMEID,
CASE 
WHEN CAST(account_type AS NVARCHAR(20)) IN (''A'',''L'',''O'')  THEN (CAST(begin_balance_dr AS DECIMAL(25,10)) - CAST(begin_balance_cr AS DECIMAL(25,10))) + (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
WHEN CAST(account_type AS NVARCHAR(20))IN (''R'',''E'') THEN (CAST(period_net_dr AS DECIMAL(25,10))    - CAST(period_net_cr AS DECIMAL(25,10))) 
ELSE (CAST(period_net_dr AS DECIMAL(25,10)) - CAST(period_net_cr AS DECIMAL(25,10)))
END SIGNEDDATA,
LAST_UPDATE_DATE as LUPDATE		
FROM openquery(PROD_LINK,''   SELECT ledger_id, GLB.last_update_date, GLB.code_combination_id,
begin_balance_dr, begin_balance_cr, period_net_dr, period_net_cr, period_name,
GLB.period_num, GLB.period_year, currency_code, actual_flag, segment1, segment2,
segment3, segment4, segment5, segment6, segment7, segment8, segment9,
segment10, segment11, segment12, segment13, gcc.account_type
FROM gl.gl_balances GLB, gl.gl_code_combinations gcc
WHERE GLB.period_year >= '
SELECT @TSQL1a =' AND GLB.last_update_date >= to_date('''''
SELECT @TSQL2 =''''',''''yyyy-mm-dd hh24:mi:ss'''')
AND actual_flag = ''''A''''
AND ledger_id = 190
AND (currency_code = ''''CAD'''' OR (currency_code = ''''STAT''''
AND segment3 IN (SELECT account_id FROM xxcgc_rpt.xxcgc_rpt_z00000_acct_flath)))
AND GLB.code_combination_id = gcc.code_combination_id
AND segment1 <> ''''T''''
AND segment2 <> ''''T''''
AND segment3 <> ''''T''''
AND segment4 <> ''''T''''
AND segment5 <> ''''T''''
AND segment6 <> ''''T''''
AND segment7 <> ''''T''''
AND segment8 <> ''''T''''
AND segment9 <> ''''T''''
AND segment10 <> ''''T''''
AND segment11 <> ''''T''''
AND segment12 <> ''''T''''
AND segment13 <> ''''T''''
AND  ((glb.last_updated_by not in (select requested_by from applsys.fnd_concurrent_requests
              where concurrent_program_id = 20119
              and last_update_date > to_date('''''
SELECT @TSQL3=''''',''''YYYY-MM-DD HH24:MI:SS''''))) OR
       ( glb.last_updated_by in (select requested_by from applsys.fnd_concurrent_requests
              where concurrent_program_id = 20119
              and last_update_date > to_date('''''
Select @TSQL4=''''',''''YYYY-MM-DD HH24:MI:SS'''')) and
to_char(GLB.PERIOD_NAME) = '''''
Select @TSQL5=''''' OR ( glb.last_updated_by in (select requested_by from applsys.fnd_concurrent_requests
              where concurrent_program_id = 20119
              and last_update_date > to_date('''''        
Select @TSQL6=''''',''''YYYY-MM-DD HH24:MI:SS'''')) and               
to_char(GLB.PERIOD_NAME) = '''''
SELECT @TSQL7=''''')))'')
               WHERE CAST(period_year AS NVARCHAR(20)) >= (SELECT Value FROM tblDefaults WHERE KeyID = ''PERIOD_YEAR_TO_LOAD'')
               AND SEGMENT6  <> ''T_PRG''
			AND SEGMENT5  <> ''T_PRV''
			AND SEGMENT11 <> ''T_TRT''
			AND SEGMENT2  <> ''T_CCT''
			AND SEGMENT7  <> ''T_PRD''
			AND SEGMENT12 <> ''T_REI''
			AND SEGMENT4  <> ''T_ICO''
			AND SEGMENT10 <> ''T_PRJ''
			AND SEGMENT13 <> ''T_LYR''
			AND SEGMENT8  <> ''T_LOB''
			AND SEGMENT9  <> ''T_PAR'''


BEGIN
IF NOT EXISTS(SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'aBPC_STAGE_INC')


CREATE TABLE dbo.aBPC_STAGE_INC(--CREATES TABLE IF IT DOESN'T EXIST
[LEDGER_ID]   NVARCHAR(20),
[COMPANY]     NVARCHAR(20),
[COSTCENTRE]  NVARCHAR(20),
[ACCOUNT]     NVARCHAR(20),
[INTCO]       NVARCHAR(20),
[PROVINCE]    NVARCHAR(20),
[PROGRAM]     NVARCHAR(20),
[PRODUCT]     NVARCHAR(20),
[LOB]         NVARCHAR(20),
[PAR]         NVARCHAR(20),
[PROJECT]     NVARCHAR(20),
[TREATY]      NVARCHAR(20),
[REINSURER]   NVARCHAR(20),
[LOSSYEAR]    NVARCHAR(20),
[A_TYPE]      NVARCHAR(20),
[CURRENCY]    NVARCHAR(20),
[A_FLAG]      NVARCHAR(20),
[PERIOD_NAME] NVARCHAR(20),
[PERIOD_YEAR] NVARCHAR(20),
[PERIOD_NUM]  NVARCHAR(20),
[SOURCE]      NVARCHAR(20),
[DATASRC]     NVARCHAR(20),
[CATEGORY]    NVARCHAR(20),
[TIME]        NVARCHAR(20),
[TIMEID]      NVARCHAR(20),
[SIGNEDDATA]  DECIMAL(25,10),
[LUPDATE] datetime)

ELSE
TRUNCATE TABLE DBO.aBPC_STAGE_INC
END


BEGIN
set @TSQL8 = cast(@TSQL AS nvarchar(MAX)) + cast(@PYEAR AS nvarchar(MAX)) +
             cast(@TSQL1a AS nvarchar(MAX))+ cast(@LR AS nvarchar(MAX)) +
             cast(@TSQL2 AS nvarchar(MAX))+ cast(@LR AS nvarchar(MAX)) +
             cast(@TSQL3 AS nvarchar(MAX))+ cast(@LR AS nvarchar(MAX)) +
             cast(@TSQL4 AS nvarchar(MAX))+ cast(@curmth AS nvarchar(MAX)) +
             cast(@TSQL5 AS nvarchar(MAX))+ cast(@LR AS nvarchar(MAX)) +
             cast(@TSQL6 AS nvarchar(MAX))+CAST(@premth AS nvarchar(MAX)) +
             cast(@TSQL7 AS nvarchar(MAX))
             
EXEC sp_executesql @TSQL8

-- added this code to keep stats on record counts of the runs
-- of the load package

INSERT into [dbo].[aBPC_LOAD_STATS] ([RecCount],[Run_Datetime])
select count(*) as RecCount, cast(floor(cast(getdate() as float(53))*24*2)/(24*2) as datetime) from aBPC_STAGE_INC a

-- added this code to correct sign incosistancies on these 3
-- stat accounts
UPDATE [dbo].[aBPC_STAGE_INC]
SET signeddata = signeddata*-1
where currency = 'STAT'
and account in ('EPPR00_ACC','FTPR00_ACC','GWPR00_ACC')


END







