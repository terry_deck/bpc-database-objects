﻿




Create procedure [dbo].[A_CLEAR_TEMPCAT] @whatever nvarchar(10)

as	

/*
A_CLEAR_TEMPCAT Stored Procedure
This procedure will delete all records in the LOGICTEMPCAT categories.
It is intended to be run after logic (primarily allocations) have written to these temp categories.
By deleting these temporary records we will not fill up the writeback table with clutter.

Copyright 2013 Column5 Consulting
Created by Stefan Dunhem
Contact:  sdunhem@column5.com
*/

--Delete records

delete from dbo.tblFACTWBConsol
WHERE CATEGORY in ('LOGICTEMPCAT1','LOGICTEMPCAT2','LOGICTEMPCAT3','LOGICTEMPCAT4')
delete from dbo.tblFAC2Consol
WHERE CATEGORY in ('LOGICTEMPCAT1','LOGICTEMPCAT2','LOGICTEMPCAT3','LOGICTEMPCAT4')
delete from dbo.tblFACTConsol
WHERE CATEGORY in ('LOGICTEMPCAT1','LOGICTEMPCAT2','LOGICTEMPCAT3','LOGICTEMPCAT4')

--End of Stored Procedure



