﻿



CREATE procedure [dbo].[A_APPSET_ON]

as	

--Appset Status On Stored Procedure
--This stored procedure sets the application set status to ON.
--It should be run at the end of a processing cycle.

UPDATE tblDefaults
SET Value='1'
WHERE KeyID='AVAILABLEFLAG'


