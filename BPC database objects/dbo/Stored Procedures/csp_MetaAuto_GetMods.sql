﻿CREATE PROC [dbo].[csp_MetaAuto_GetMods] (
	  @control_id nvarchar(50)  -- from zzzMetaControl
	, @main_log_id int =1 -- the main audit log id
) as

PRINT 'Starting to run [csp_MetaAuto_GetMods] with @control_id=' + @control_id
--set nocount on

declare
	  @src_tbl sysname --  'metaSrcCustomer'
	, @tmp_tbl sysname  -- 'metaTmpCustomer'
	, @trg_tbl sysname -- 'mbrCustmoer'
	, @mods_tbl sysname -- the table to capture the mods
	, @return_value int -- generic return value
	, @step smallint
	, @proc_log_id int -- current stored proc audit log
	, @dim_nm nvarchar(125)

EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_GetMods
	, @action_item = N'get control table names'

-- get table names
EXEC	@return_value = [dbo].[csp_MetaAuto_GetTblNames]
		@control_id = @control_id,
		@src_tbl = @src_tbl OUTPUT,
		@tmp_tbl = @tmp_tbl OUTPUT,
		@trg_tbl = @trg_tbl OUTPUT,
		@dim_nm = @dim_nm OUTPUT

EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log


EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_GetMods
	, @action_item = N'create mods table'

-- generate a mods table
EXEC	@return_value = [dbo].[csp_MetaAuto_MakeModsTbl]
		@src_tbl = @src_tbl,
		@trg_tbl = @trg_tbl,
		@unique_id = @main_log_id,
		@mods_tbl = @mods_tbl OUTPUT

EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log

if (select UPPER(isnull(PROCESS_UPD,'')) FROM mbrzzzMetaControl where ID = @control_id) in ('Y','YES', '1')
begin
	EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_GetMods
		, @action_item = N'find updated properties'

	-- add changed records
	EXEC	@return_value = [dbo].[csp_MetaAuto_GetUpd]
			@src_tbl = @tmp_tbl,
			@trg_tbl = @trg_tbl,
			@control_id = @control_id,
			@dim_nm = @dim_nm,
			@mods_tbl = @mods_tbl

	EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log
end

if (select UPPER(isnull(PROCESS_INS,'')) FROM mbrzzzMetaControl where ID = @control_id) in ('Y','YES', '1')
begin
	EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_GetMods
		, @action_item = N'find new members'

	-- add new records
	EXEC	[dbo].[csp_MetaAuto_GetNew]
			@tmp_tbl = @tmp_tbl,
			@trg_tbl = @trg_tbl,
			@dim_nm = @dim_nm,
			@mods_tbl = @mods_tbl

	EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log
end

if (select UPPER(isnull(PROCESS_DEL,'')) FROM mbrzzzMetaControl where ID = @control_id) in ('Y','YES', '1')
begin
	EXEC @proc_log_id = csp_Aud_ProcLogAdd @main_aud_log_id = @main_log_id, @proc_name = csp_MetaAuto_GetMods
		, @action_item = N'find deleted members'

	-- add deleted records
	EXEC	@return_value = [dbo].[csp_MetaAuto_GetDel]
			@tmp_tbl = @tmp_tbl,
			@trg_tbl = @trg_tbl,
			@dim_nm = @dim_nm,
			@mods_tbl = @mods_tbl

	EXEC csp_Aud_ProcLogUpd @proc_log_id  -- update log
end


-- return the list of changes
exec (N'select * from ' + @mods_tbl)
print @mods_tbl


-- return a descriptive list of changes
exec(N'
	select ''In the '' + dim_nm + '' dimension, '' + fld_nm + '' for ID:'' + id + '' is changing from '' + dst_val + '' to '' + src_val mod_description
		from ' + @mods_tbl + N'
		where mod_type = ''UPD''
	union all
	select ''In the '' + dim_nm + '' dimension, a new record is being added with ID:'' + id
		from ' + @mods_tbl + N'
		where mod_type = ''ADD''
	union all
	select ''In the '' + dim_nm + '' dimension, a record is being deleted with ID:'' + id
		from ' + @mods_tbl + N'
		where mod_type = ''DEL'''
)

-- clean up
--exec(N'drop table ' + @mods_tbl)

--[csp_MetaAuto_GetMods] @control_id='HRPosition1'


