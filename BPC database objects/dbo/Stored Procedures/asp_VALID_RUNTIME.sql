﻿


------------------------------asp_VALID_RUNTIME------------------------------------
--- 14/11/2012 T Deck
--- Created New stored procedure.  This procedure is used by the incremental 
--- data load process.  It checks to see if the current run time is between 
--- the start run time stored in the tblDefaults table under the keyid 
--- ‘BATCH_START_TIME and the end run time stored in the tblDefaults table under
--- the keyid 'BATCH_END_TIME'
-----------------------------------------------------------------------------------

CREATE procedure [dbo].[asp_VALID_RUNTIME]

AS
DECLARE @STARTTIME float
DECLARE @ENDTIME float
DECLARE @RUNTIME float
DECLARE @VALID int


BEGIN
SELECT @STARTTIME = CAST(CAST(CAST(getdate() as date) as datetime)as float) + CAST(CAST(CAST(VALUE as time)as datetime)as float) FROM tbldefaults Where KEYID = 'BATCH_START_TIME'
SELECT @ENDTIME = CAST(CAST(CAST(getdate() as date) as datetime)as float) + CAST(CAST(CAST(VALUE as time)as datetime)as float) FROM tbldefaults Where KEYID = 'BATCH_END_TIME'
SELECT @RUNTIME = CAST( getdate() AS FLOAT )

print @ENDTIME
print @RUNTIME

IF (@RUNTIME > @STARTTIME and @RUNTIME < @ENDTIME )
	BEGIN
	SELECT @VALID = 1
	END
ELSE 
	BEGIN
	SELECT @VALID = 0
	END

Select @VALID as vcount
print @VALID
END



