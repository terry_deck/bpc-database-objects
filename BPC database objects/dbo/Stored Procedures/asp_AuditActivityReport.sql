﻿


CREATE PROCEDURE [dbo].[asp_AuditActivityReport]( 
@WFrom DATETIME, 
@WTo DATETIME)
AS
 
Begin
SELECT     A.ActivityHdrID, DATEADD(hour, -6,a.TimeStamp) TimeStamp, a.App, a.UserID, a.MachineID, a.FunctionalTask,
           a.Source, a.Param, a.Succeeded, a.Duration, a.ReturnMsg,
		   b.ActivityHeaderID, b.ActivityType, b.Field, b.PreviousValue,
		   b.NewValue, a.ActivityKind
FROM    [PROD_COBPCExtra].[dbo].[AuditActivityHdrPROD_COBPC] A, 
              [PROD_COBPCExtra].[dbo].[AuditActivityDetailPROD_COBPC] B 
WHERE    A.[ActivityHdrid] = B.[ActivityHeaderID] 
AND   DATEADD(hour,-6,a.[TimeStamp]) BETWEEN CONVERT(NVARCHAR(30),@WFrom,9) And CONVERT(NVARCHAR(30),@WTo +1,9)
AND ActivityKind = 'ADMIN'
End


