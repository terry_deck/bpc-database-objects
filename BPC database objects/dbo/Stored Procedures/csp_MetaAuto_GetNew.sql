﻿CREATE PROC [dbo].[csp_MetaAuto_GetNew] (
	  @tmp_tbl sysname 
	, @trg_tbl sysname 
	, @dim_nm nvarchar(50) 
	, @mods_tbl sysname 
) as

declare @sql nvarchar(500)

set @sql = N'insert ' + @mods_tbl + N'(mod_type, dim_nm, ID)
	select ''ADD'', N''' + @dim_nm + N''', t.ID
	from ' + @tmp_tbl + N' t
	left outer join ' + @trg_tbl + N' m
	  on m.ID = t.ID
	where m.ID is null'


-- print @sql
exec (@sql)

/* Sample call:

DECLARE	@return_value int

EXEC	@return_value = [dbo].[csp_MetaAuto_GetNew]
		@tmp_tbl = metaTmpHRPosition,
		@trg_tbl = mbrHRPosition,
		@dim_nm = N'HRPosition',
		@mods_tbl = 'metaModsNewHRPosition'

SELECT	'Return Value' = @return_value

*/
/****** Object:  StoredProcedure [dbo].[csp_MetaAuto_GetMods]    Script Date: 08/13/2008 01:17:25 ******/
SET ANSI_NULLS ON
