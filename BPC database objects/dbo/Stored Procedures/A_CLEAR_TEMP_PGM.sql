﻿




CREATE procedure [dbo].[A_CLEAR_TEMP_PGM] @passedpgm nvarchar(1000)

as	

/*
A_CLEAR_TEMP Stored Procedure
This procedure will delete all records in the LOGICTEMP data source for the selected program.
It is intended to be run after logic (primarily allocations) have written to this temp data source.
By deleting these temporary records we will not fill up the writeback table with clutter.

Copyright 2012 Column5 Consulting
Created by Stefan Dunhem
Contact:  sdunhem@column5.com
*/

-- Create a temp table and load variables passed via stored proc into a scope table.
-- For use in filtering the SQL statement later on.  This allows us to accept multiple comma delimited members.

create table #aCLEARTEMP_SCOPE (
DIMENSION NVARCHAR(20),
MEMBER NVARCHAR(20) )

DECLARE @List nvarchar(1000) -- variable to hold the list of comma delimited cost centre member ID's
DECLARE @ListItem nvarchar(20) -- variable to hold the parsed member (program)
DECLARE @Pos int  -- variable to hold the position value of the first comma in the list

-- Assign cost centre string passed to procedure to the List variable.
SET @List = @passedpgm

-- Loop while the list string still holds one or more characters.
WHILE LEN(@List) > 0
Begin
       -- Get the position of the first comma (returns 0 if no commas left in string).
       SET @Pos = CHARINDEX(',', @List)

       -- Extract the list item string.
       IF @Pos = 0
       Begin
               SET @ListItem = @List
       End
       ELSE
       Begin
               SET @ListItem = SUBSTRING(@List, 1, @Pos - 1)
       End

		-- Insert the parsed cost centre member into the scope table.
	   Insert into #aCLEARTEMP_SCOPE select 'PROGRAM', @ListItem

       -- Remove the list item (and trailing comma if present) from the list string.
       IF @Pos = 0
       Begin
               SET @List = ''
       End
       ELSE
       Begin
               -- Start substring at the character after the first comma.
                SET @List = SUBSTRING(@List, @Pos + 1, LEN(@List) - @Pos)
       End
End

----------------------------------------------------------------------------------------

--Delete records

delete from dbo.tblFACTWBPremClaim
WHERE DATASRCPC='LOGICTEMP'
AND PROGRAM in (select distinct MEMBER from #aCLEARTEMP_SCOPE where DIMENSION = 'PROGRAM')
delete from dbo.tblFAC2PremClaim
WHERE DATASRCPC='LOGICTEMP'
AND PROGRAM in (select distinct MEMBER from #aCLEARTEMP_SCOPE where DIMENSION = 'PROGRAM')
delete from dbo.tblFACTPremClaim
WHERE DATASRCPC='LOGICTEMP'
AND PROGRAM in (select distinct MEMBER from #aCLEARTEMP_SCOPE where DIMENSION = 'PROGRAM')

--Drop the scope table

DROP TABLE [#aCLEARTEMP_SCOPE]



